using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DisplayLanguage : MonoBehaviour
{
    [SerializeField]
    private TextMeshPro textMeshPro;

    [SerializeField]
    [TextArea(3, 30)]
    private string englishText;

    [SerializeField]
    [TextArea(3, 30)]
    private string germanText;

    private DisplayLanguageSelector displayLanguageSelector;

    private void Start()
    {
        textMeshPro = GetComponent<TextMeshPro>();
        DisplayLanguageSelector.GetInstance.SubscribeToLanguageChange(UpdateText);
        UpdateText();
    }

    private void OnValidate()
    {
        var tmp = GetComponent<TextMeshPro>();
        if (tmp != null)
        {
            textMeshPro = tmp;
        }
        UpdateText();
    }

    private void UpdateText()
    {
        if (textMeshPro != null)
        {
            if (DisplayLanguageSelector.GetInstance != null)
            {
                switch (DisplayLanguageSelector.GetInstance.SelectedLanguage)
                {
                    case DisplayLanguageSelector.Language.English:
                        textMeshPro.text = englishText;
                        break;
                    case DisplayLanguageSelector.Language.German:
                        textMeshPro.text = germanText;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
            else
            {
                textMeshPro.text = englishText;
            }
        }
    }

}
