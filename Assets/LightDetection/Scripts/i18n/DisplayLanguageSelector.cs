using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DisplayLanguageSelector : MonoBehaviour
{
    [SerializeField]
    private Language selectedLanguage;

    public Language SelectedLanguage { get => selectedLanguage; set => selectedLanguage = value; }

    private static DisplayLanguageSelector instance;
    public static DisplayLanguageSelector GetInstance => instance;

    [SerializeField]
    UnityEvent onLanguageChange;

    void Awake()
    {
        EnsureSingleton();
    }

    //void OnValidate()
    //{
    //    EnsureSingleton();
    //    SetLanguage();
    //}

    private void EnsureSingleton()
    {
        if (instance != null && instance != this)
        {
            Debug.LogWarning("Multiple instances of DisplayLanguageSelector exist!");
        }
        else
        {
            instance = this;
        }
    }

    public void SubscribeToLanguageChange(UnityAction action)
    {
        onLanguageChange.AddListener(action);
    }

    public void SetLanguage()
    {
        SetLanguage(selectedLanguage);
    }

    public void SetLanguage(Language language)
    {
        selectedLanguage = language;
        onLanguageChange.Invoke();
        name = "i8n - " + language.ToString();
    }

    public enum Language
    {
        English,
        German
    }
}
