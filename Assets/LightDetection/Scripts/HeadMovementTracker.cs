using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadMovementTracker : MonoBehaviour
{
    [SerializeField]
    [Range(0, 40)]
    private int threshold = 20;

    [SerializeField]
    [Range(0f, 1f)]
    private float detectionSensitivity = .5f;

    [SerializeField]
    [Range(0f, 1f)]
    private float fallOffSensitivity = .2f;

    private static bool moving = false;

    public static bool Moving { get {
            if (instance != null)
                return moving;
            else
                throw new System.NullReferenceException("No instance of type HeadMovementTracker available.");
        } 
    }

    [SerializeField]
    private float debugSpeed;

    private static float headMovementSpeed = 0;

    private static HeadMovementTracker instance;

    private Quaternion priorHeadOrientation;

    // Start is called before the first frame update
    void Start()
    {
        if (instance == null)
        {
            instance = this;
        } else
        {
            Debug.LogWarning("Singleton: Too many instances of type HeadMovementTracker.");
            this.enabled = false;
        }

        priorHeadOrientation = Camera.main.transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        UpdateHeadMovementSpeed();
    }

    private void UpdateHeadMovementSpeed()
    {
        Quaternion currentHeadOrientation = Camera.main.transform.rotation;

        float angularVelocity = Quaternion.Angle(priorHeadOrientation, currentHeadOrientation) / Time.deltaTime;

        if (angularVelocity > headMovementSpeed)
        {
            // Detect movement quickly
            headMovementSpeed = angularVelocity * detectionSensitivity + headMovementSpeed * (1 - detectionSensitivity);
        }
        else
        {
            // Fade out movement slowly
            headMovementSpeed = (angularVelocity * fallOffSensitivity + headMovementSpeed * (1 - fallOffSensitivity));
        }

        if (headMovementSpeed < 1)
        {
            // Ignore insignificant values
            headMovementSpeed = 0;
        }

        moving = headMovementSpeed > threshold;

        debugSpeed = headMovementSpeed;

        priorHeadOrientation = Camera.main.transform.rotation;
    }

}
