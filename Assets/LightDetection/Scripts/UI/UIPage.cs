using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIPage : MonoBehaviour
{
    [SerializeField]
    private bool isVisible = false;

    // Start is called before the first frame update
    void Start()
    {
        if (!isVisible)
        {
            Hide();
        }
    }

    internal void Hide()
    {
        isVisible = false;
        gameObject.SetActive(false);
    }

    internal void Show()
    {
        isVisible = true;
        gameObject.SetActive(true);
    }

    public void HideDelayed(float delay = .15f)
    {
        StartCoroutine(HideDelayedCoroutine(delay));
    }

    private IEnumerator HideDelayedCoroutine(float delay)
    {
        yield return new WaitForSeconds(delay);
        Hide();
    }

}
