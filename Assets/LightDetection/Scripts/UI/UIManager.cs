using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Class that manages different predefined views of the UI and the transitions between them. additionally it positions new views in front of the user
public class UIManager : MonoBehaviour
{
    [Header("View Prefabs")]

    [SerializeField]
    private List<UIView> uiViews;
    
    private Dictionary<ViewType, UIView> specialViews = new Dictionary<ViewType, UIView>();
    
    public enum ViewType
    {
        Content,
        MainMenu,
        Settings,
        Credits,
        Help
    }

    private ViewType currentView = ViewType.Content;

    private static UIManager instance;

    public static UIManager GetInstance => instance;

    private void Awake()
    {
        EnsureSingleton();
        // SetView(ViewType.MainMenu);

        // Add all uiviews to the views dictionary
        foreach (UIView view in uiViews)
        {
            if (view.ViewType != ViewType.Content)
            {
                specialViews.Add(view.ViewType, view);
            }
        }
    }

    private void EnsureSingleton()
    {
        if (instance != null && instance != this)
        {
            Debug.LogWarning("Multiple instances of UIManager exist!");
            enabled = false;
        }
        else
        {
            // Save the instance
            instance = this;
        }
    }

    public void RegisterView(UIView view)
    {
        if (!uiViews.Contains(view))
        {
            uiViews.Add(view);
        }

        if (!specialViews.ContainsKey(view.ViewType))
        {
            specialViews.Add(view.ViewType, view);
        }
    }

    public void SetContentView(UIView content)
    {
        HideAllViews();

        content.Show();

        currentView = ViewType.Content;
    }

    private void SetView(ViewType viewType, bool hideAllOtherViews = true)
    {
        if (hideAllOtherViews)
        {
            HideAllViews();
        }

        currentView = viewType;

        if (currentView != ViewType.Content && specialViews[currentView] != null)
        {
            specialViews[currentView].Show();
        }
    }

    public void HideAllViews()
    {
        Debug.Log("HideAllViews");

        foreach (UIView view in uiViews)
        {
            if (view.isActiveAndEnabled)
            {
                view.HideDelayed();
            }
        }
    }
}

