using System;
using System.Collections;
using UnityEngine;

public class UIView : MonoBehaviour
{
    [SerializeField]
    private UIManager.ViewType viewType;

    public UIManager.ViewType ViewType { get => viewType; set => viewType = value; }

    [SerializeField]
    private UIPage[] pages;

    private int currentPage = 0;

    [SerializeField]
    private bool isVisible = false;

    private const float PAGE_CHANGE_DELAY = .15f;

    private void Start()
    {
        RegisterView();

        if (isVisible)
        {
            HideAllPages();
            ShowPage(0);
        } else
        {
            Hide();
        }
        
    }

    private void RegisterView()
    {
        UIManager.GetInstance.RegisterView(this);
    }

    public void Show()
    {
        transform.position = Camera.main.transform.position + Camera.main.transform.forward * .65f;
        isVisible = true;
        gameObject.SetActive(true);
        ShowPage(0);
    }

    public void Hide()
    {
        isVisible = false;
        gameObject.SetActive(false);
    }

    public void ShowNextPage()
    {
        ShowPageDelayed(currentPage + 1);
    }

    public void ShowPreviousPage()
    {
        ShowPageDelayed(currentPage - 1);
    }

    public void ShowPage(int index)
    {
        if (index >= 0 && index < pages.Length)
        {
            pages[currentPage].Hide();
            currentPage = index;
            pages[currentPage].Show();
        }
    }

    public void ShowPageDelayed(int index, float delay = PAGE_CHANGE_DELAY)
    {
        StartCoroutine(ShowPageDelayedCoroutine(index, delay));
    }

    public void ShowPageDelayed(int index)
    {
        ShowPageDelayed(index, PAGE_CHANGE_DELAY);
    }

    private IEnumerator ShowPageDelayedCoroutine(int index, float delay)
    {
        if (index >= 0 && index < pages.Length)
        {
            pages[currentPage].HideDelayed(delay);
            yield return new WaitForSeconds(delay + .1f);
            ShowPage(index);
        }
    }

    public void HideAllPages()
    {
        foreach (var page in pages)
        {
            if (page.isActiveAndEnabled)
            {
                page.Hide();
            }
        }
    }

    public void HideDelayed(float delay = PAGE_CHANGE_DELAY)
    {
        StartCoroutine(HideDelayedCoroutine(delay));
    }

    private IEnumerator HideDelayedCoroutine(float delay)
    {
        yield return new WaitForSeconds(delay);
        Hide();
    }
}
