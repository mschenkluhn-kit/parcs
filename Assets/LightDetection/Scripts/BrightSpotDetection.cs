using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using OpenCVForUnity.CoreModule;
using OpenCVForUnity.ImgprocModule;
using OpenCVForUnity.UnityUtils.Helper;
using OpenCVForUnity.UnityUtils;
using HoloLensWithOpenCVForUnity.UnityUtils.Helper;
using HoloLensCameraStream;
using Microsoft.MixedReality.Toolkit;

namespace HoloLensWithOpenCVForUnityExample
{
    /// <summary>
    /// Based on HLCameraStreamToMatHelper Example, an example of image processing (comic filter) using OpenCVForUnity on Hololens.
    /// Referring to http://dev.classmethod.jp/smartphone/opencv-manga-2/.
    /// </summary>
    [RequireComponent(typeof(HLCameraStreamToMatHelper))]
    public class BrightSpotDetection : MonoBehaviour
    {
        [Header("Bright Spot Detection Features")]

        [SerializeField]
        private bool showCameraFrame = false;

        [SerializeField]
        private bool showDetectionIndicators = true;

        [SerializeField]
        private bool findTrackingFeatures = false;

        [SerializeField]
        [Range(200, 254)]
        private int minBrightness = 245;

        [SerializeField]
        [Range(200, 255)]
        private int maxBrightness = 255;

        [SerializeField]
        private BrightSpotManager brightSpotManager;

        /// <summary>
        /// The texture.
        /// </summary>
        Texture2D texture;

        /// <summary>
        /// The quad renderer.
        /// </summary>
        Renderer quad_renderer;

        /// <summary>
        /// The web cam texture to mat helper.
        /// </summary>
        HLCameraStreamToMatHelper frontCamera;

        [Space(10)]

        [HeaderAttribute("Debug")]

        public Text renderFPS;
        public Text videoFPS;
        public Text trackFPS;
        public Text debugStr;
        public Text debugStr2;

        private bool active = false;

        protected void Start()
        {
            frontCamera = gameObject.GetComponent<HLCameraStreamToMatHelper>();
            frontCamera.outputColorFormat = WebCamTextureToMatHelper.ColorFormat.BGRA;
            frontCamera.Initialize();

            frontCamera.Stop();
        }

        public void StartDetection()
        {
            if (!active)
            {
                frontCamera.Play();
                active = true;
            }
        }

        public void StopDetection()
        {
            if (active)
            {
                frontCamera.Stop();
                active = false;
            }
        }

        /// <summary>
        /// Raises the web cam texture to mat helper initialized event.
        /// </summary>
        public void OnWebCamTextureToMatHelperInitialized()
        {
            Debug.Log("OnWebCamTextureToMatHelperInitialized");

            Mat bgraMat = frontCamera.GetMat();

            texture = new Texture2D(bgraMat.cols(), bgraMat.rows(), TextureFormat.BGRA32, false);
            texture.wrapMode = TextureWrapMode.Clamp;
            quad_renderer = gameObject.GetComponent<Renderer>() as Renderer;
            quad_renderer.sharedMaterial.SetTexture("_MainTex", texture);


            //Debug.Log("Screen.width " + Screen.width + " Screen.height " + Screen.height + " Screen.orientation " + Screen.orientation);

            DebugUtils.AddDebugStr(frontCamera.outputColorFormat.ToString() + " " + frontCamera.GetWidth() + " x " + frontCamera.GetHeight() + " : " + frontCamera.GetFPS());


            Matrix4x4 projectionMatrix;
#if WINDOWS_UWP && !DISABLE_HOLOLENSCAMSTREAM_API
            projectionMatrix = frontCamera.GetProjectionMatrix();
            quad_renderer.sharedMaterial.SetMatrix("_CameraProjectionMatrix", projectionMatrix);
#else
            // This value is obtained from PhotoCapture's TryGetProjectionMatrix() method. I do not know whether this method is good.
            // Please see the discussion of this thread. Https://forums.hololens.com/discussion/782/live-stream-of-locatable-camera-webcam-in-unity
            projectionMatrix = Matrix4x4.identity;
            projectionMatrix.m00 = 2.31029f;
            projectionMatrix.m01 = 0.00000f;
            projectionMatrix.m02 = 0.09614f;
            projectionMatrix.m03 = 0.00000f;
            projectionMatrix.m10 = 0.00000f;
            projectionMatrix.m11 = 4.10427f;
            projectionMatrix.m12 = -0.06231f;
            projectionMatrix.m13 = 0.00000f;
            projectionMatrix.m20 = 0.00000f;
            projectionMatrix.m21 = 0.00000f;
            projectionMatrix.m22 = -1.00000f;
            projectionMatrix.m23 = 0.00000f;
            projectionMatrix.m30 = 0.00000f;
            projectionMatrix.m31 = 0.00000f;
            projectionMatrix.m32 = -1.00000f;
            projectionMatrix.m33 = 0.00000f;
            quad_renderer.sharedMaterial.SetMatrix("_CameraProjectionMatrix", projectionMatrix);
#endif

            float halfOfVerticalFov = Mathf.Atan(1.0f / projectionMatrix.m11);
            float aspectRatio = (1.0f / Mathf.Tan(halfOfVerticalFov)) / projectionMatrix.m00;
        }

        /// <summary>
        /// Raises the web cam texture to mat helper disposed event.
        /// </summary>
        public void OnWebCamTextureToMatHelperDisposed()
        {
            Debug.Log("OnWebCamTextureToMatHelperDisposed");

            if (debugStr != null)
            {
                debugStr.text = string.Empty;
            }
            DebugUtils.ClearDebugStr();
        }

        private void DetectFeatures(Mat frame, out Mat res)
        {
            res = new Mat(frame.size(), frame.type(), new Scalar(0, 0, 0, 0));
            Mat mask = Mat.zeros(frame.size(), frame.type());

            // Light detection per frame
            Imgproc.cvtColor(frame, mask, Imgproc.COLOR_RGBA2GRAY);

            Imgproc.blur(mask, mask, new Size(5, 5));
            Core.inRange(mask, new Scalar(minBrightness), new Scalar(maxBrightness), mask);

            if (showCameraFrame)
            {
                Core.bitwise_and(frame, frame, res);
            }

            // Tracking Features
            if (findTrackingFeatures)
            {
                MatOfPoint matOfPoint = new MatOfPoint();
                Imgproc.goodFeaturesToTrack(mask, matOfPoint, 100, .8, 300);

                for (int i = 0; i < matOfPoint.rows(); i++)
                {
                    for (int j = 0; j < matOfPoint.cols(); j++)
                    {
                        //Debug.Log(matOfPoint.get(i, j)[0] + " " + matOfPoint.get(i, j)[1]);
                        Imgproc.circle(res, new Point(matOfPoint.get(i, j)), 5, new Scalar(255), -1);
                    }
                }
            }

            Imgproc.blur(mask, mask, new Size(50, 50));

            // Contours
            List<MatOfPoint> contours = new List<MatOfPoint>();
            Imgproc.findContours(mask, contours, new Mat(), Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);

            if (showDetectionIndicators)
            {
                Imgproc.drawContours(res, contours, -1, new Scalar(0, 255, 0), 2);
            }

            foreach (MatOfPoint contour in contours)
            {
                Moments m = Imgproc.moments(contour);

                int cx = (int)(Math.Round(m.get_m10() / m.get_m00()));
                int cy = (int)(Math.Round(m.get_m01() / m.get_m00()));

                if (showDetectionIndicators)
                {
                    Imgproc.circle(res, new Point(cx, cy), 5, new Scalar(0, 0, 255), -1);
                }

                DetectFeaturePosition(cx, cy, out Vector3? position);
                if (position.HasValue)
                {
                    CorrectFeaturePositionByCameraAngle(position.Value, out Vector3 correctedPosition);
                    brightSpotManager.AddBrightSpotPosition(correctedPosition);
#if UNITY_EDITOR
                    Debug.DrawRay(position.Value, correctedPosition - position.Value, Color.red, 1f);
#endif
                }

            }
        }

        private void DetectFeaturePosition(int cx, int cy, out Vector3? position)
        {
            position = null;

            Transform cam = Camera.main.transform;
            Vector3 pos = new Vector3((1f * cx / frontCamera.requestedWidth - .5f) * transform.localScale.x, (1f * -cy / frontCamera.requestedHeight + .5f) * transform.localScale.y, 2.2f);

            pos += cam.position;
            pos = pos.RotateAround(cam.position, cam.rotation);

            Physics.Raycast(cam.position, pos - cam.position, out RaycastHit hit, 15, 1 << 31); // 1 << 31 creates a layerMask for the SpatialAwareness layer via bitshift

#if UNITY_EDITOR
            Debug.DrawRay(cam.position, pos - cam.position, Color.green, 1f);
#endif

            if (hit.transform != null)
            {
                position = hit.point;
            }
        }

        private void CorrectFeaturePositionByCameraAngle(Vector3 position, out Vector3 correctedPosition)
        {
            correctedPosition = position;

            if (position.y > Camera.main.transform.position.y)
            {
                Vector3 cameraToPosition = position - Camera.main.transform.position;

                float angle = Mathf.Min(180 - 2 * Vector3.Angle(cameraToPosition, Vector3.up), 180);

                Vector3 rotationAxis = Vector3.Cross(Vector3.up, cameraToPosition);

                correctedPosition = position + Quaternion.AngleAxis(angle, rotationAxis) * cameraToPosition.normalized * cameraToPosition.magnitude * .1f;
            }

        }

        /// <summary>
        /// Raises the web cam texture to mat helper error occurred event.
        /// </summary>
        /// <param name="errorCode">Error code.</param>
        public void OnWebCamTextureToMatHelperErrorOccurred(WebCamTextureToMatHelper.ErrorCode errorCode)
        {
            Debug.Log("OnWebCamTextureToMatHelperErrorOccurred " + errorCode);
            debugStr2.text = "OnWebCamTexture Error: " + errorCode.ToString();
        }
        void Update()
        {
            if (active)
            {
                if (frontCamera.IsPlaying() && frontCamera.DidUpdateThisFrame())
                {
                    DebugUtils.VideoTick();

                    Mat frame = frontCamera.GetMat();

                    if (!HeadMovementTracker.Moving)
                    {
                        DetectFeatures(frame, out frame);
                    }

                    DebugUtils.TrackTick();

                    // For BGRA or BGR format, use the fastMatToTexture2D method.
                    Utils.fastMatToTexture2D(frame, texture);
                }

                if (frontCamera.IsPlaying())
                {

                    Matrix4x4 cameraToWorldMatrix = frontCamera.GetCameraToWorldMatrix();
                    Matrix4x4 worldToCameraMatrix = cameraToWorldMatrix.inverse;

                    quad_renderer.sharedMaterial.SetMatrix("_WorldToCameraMatrix", worldToCameraMatrix);

                    // Position the canvas object slightly in front
                    // of the real world web camera.
                    Vector3 position = cameraToWorldMatrix.GetColumn(3) - cameraToWorldMatrix.GetColumn(2) * 2.2f;

                    // Rotate the canvas object so that it faces the user.
                    Quaternion rotation = Quaternion.LookRotation(-cameraToWorldMatrix.GetColumn(2), cameraToWorldMatrix.GetColumn(1));

                    gameObject.transform.SetPositionAndRotation(position, rotation);
                }
            }
        }

        void LateUpdate()
        {
            DebugUtils.RenderTick();
            float renderDeltaTime = DebugUtils.GetRenderDeltaTime();
            float videoDeltaTime = DebugUtils.GetVideoDeltaTime();
            float trackDeltaTime = DebugUtils.GetTrackDeltaTime();

            if (renderFPS != null)
            {
                renderFPS.text = string.Format("Render: {0:0.0} ms ({1:0.} fps)", renderDeltaTime, 1000.0f / renderDeltaTime);
            }
            if (videoFPS != null)
            {
                videoFPS.text = string.Format("Video: {0:0.0} ms ({1:0.} fps)", videoDeltaTime, 1000.0f / videoDeltaTime);
            }
            if (trackFPS != null)
            {
                trackFPS.text = string.Format("Track:   {0:0.0} ms ({1:0.} fps)", trackDeltaTime, 1000.0f / trackDeltaTime);
            }
            if (debugStr != null)
            {
                if (DebugUtils.GetDebugStrLength() > 0)
                {
                    if (debugStr.preferredHeight >= debugStr.rectTransform.rect.height)
                        debugStr.text = string.Empty;

                    debugStr.text += DebugUtils.GetDebugStr();
                    DebugUtils.ClearDebugStr();
                }
            }
        }

        /// <summary>
        /// Raises the destroy event.
        /// </summary>
        void OnDestroy()
        {
            frontCamera.Dispose();
        }

        #region Button Interactions

        /// <summary>
        /// Raises the back button click event.
        /// </summary>
        public void OnBackButtonClick()
        {
            SceneManager.LoadScene("HoloLensWithOpenCVForUnityExample");
        }

        /// <summary>
        /// Raises the play button click event.
        /// </summary>
        public void OnPlayButtonClick()
        {
            frontCamera.Play();
        }

        /// <summary>
        /// Raises the pause button click event.
        /// </summary>
        public void OnPauseButtonClick()
        {
            frontCamera.Pause();
        }

        /// <summary>
        /// Raises the stop button click event.
        /// </summary>
        public void OnStopButtonClick()
        {
            frontCamera.Stop();
        }

        /// <summary>
        /// Raises the change camera button click event.
        /// </summary>
        public void OnChangeCameraButtonClick()
        {
            frontCamera.requestedIsFrontFacing = !frontCamera.requestedIsFrontFacing;
        }

        #endregion
    }
}