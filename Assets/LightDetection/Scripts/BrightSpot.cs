using HoloLensWithOpenCVForUnityExample;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrightSpot : MonoBehaviour
{
    [SerializeField]
    private Color color;

    [SerializeField]
    [Tooltip("When is a detection considered as a bright spot.")]
    private int confirmationThreshold = 5;

    [SerializeField]
    [Tooltip("Determines when additional detections should be dropped. Position is not updated afterwards. 0 to ignore property")]
    private int updateThreshold = 0;

    [SerializeField]
    [Tooltip("Determines how long a bright spot will be considered as active.")]
    private int maxLifePoints = 3;

    [SerializeField]
    [Tooltip("How often per second will alive status be checked.")]
    private int lifeUpdateRate = 5;

    private int updates = 0;

    [SerializeField]
    private int lifePoints = 0;

    private bool detected = true;
    private bool relevant = false;

    public Color Color
    {
        get => color;
        set
        {
            if (gameObject.GetComponent<Renderer>() != null)
            {
                Material mat = Material.Instantiate(gameObject.GetComponent<Renderer>().material);
                mat.color = value;
                gameObject.GetComponent<Renderer>().material = mat;
            }
            color = value;
        }
    }
    public Vector3 Position => gameObject.transform.position;

    /// <summary>
    /// Spot was detected multiple times to confirm its existance. Depends on confirmationThreshold.
    /// </summary>
    public bool Confirmed => updates > confirmationThreshold;

    public bool Relevant { get => relevant; internal set => relevant = value; }

    public bool Alive => lifePoints > 0;

    public void Start()
    {
        lifePoints = maxLifePoints;

        StartCoroutine(UpdateHealth());
    }

    public void UpdatePositionTowards(Vector3 newPosition)
    {
        detected = true;

        if (++updates > updateThreshold && updateThreshold != 0)
        {
            // No more updates accepted
            return;
        }

        Vector3 pos = gameObject.transform.position;

        // Average position with new information
        gameObject.transform.position = pos + (newPosition - pos) * (1f / updates);
    }

    private IEnumerator UpdateHealth()
    {
        while (true)
        {
            if (GetComponent<Renderer>().isVisible && !HeadMovementTracker.Moving)
            {
                if (detected)
                {
                    lifePoints++;
                    detected = false;
                    if (lifePoints > maxLifePoints)
                        lifePoints = maxLifePoints;
                }
                else if (lifePoints > 0)
                {
                    lifePoints--;
                }

                if (Alive)
                {
                    this.Color = Color.green;
                }
                else
                {
                    this.Color = Color.red;
                }
            } else
            {
                this.Color = Color.blue;
            }

            

            yield return new WaitForSeconds(1f / lifeUpdateRate);
        }
    }

}
