using HoloLensWithOpenCVForUnityExample;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;

public class LightDetection : MonoBehaviour
{
    [SerializeField]
    [Tooltip("Delay until the light is turned on after it was turned off.")]
    private float turnOnDelay = 2f;

    [SerializeField]
    [Tooltip("Delay until the light is turned off after it was turned on.")]
    private float turnOffDelay = 2f;

    [SerializeField]
    [Tooltip("Minimum number of light cycles (off > on > off) per light.")]
    private float minimumCyclesPerLight = 3;

    [SerializeField]
    [Tooltip("Maximum number of light cycles (off > on > off) per light until process is aborted.")]
    private float maximumCyclesPerLight = 12;

    [SerializeField]
    private GameObject lightPrefab;

    [Header("Service References")]

    [SerializeField]
    private HomeAssistantAreaManager homeAssistantAreaManager;

    [SerializeField]
    private BrightSpotManager brightSpotManager;

    [SerializeField]
    private BrightSpotDetection brightSpotDetection;

    [SerializeField]
    private HomeAssistantConnector homeAssistantConnctor;

    [Header("Debug")]

    [SerializeField]
    private bool mockHomeAssistantActions;

    public void Start()
    {
    }

    private readonly HomeAssistantTemplateData lightTemplate = new HomeAssistantTemplateData { template = "{% set areas = states | selectattr('attributes.device_class', 'undefined') | map(attribute='entity_id') | reject('none') | list %}[{% for eid in areas %}{% if eid.startswith('light') and not 'group' in eid and area_id(eid) %}{\"entity_id\": \"{{ eid }}\", \"area_id\": \"{{ area_id(eid) }}\"},{% endif %}{% endfor %}]" };

    public IEnumerator StartDetection(HomeAssistantArea area)
    {
        yield return new WaitForSeconds(.5f);

        if (!mockHomeAssistantActions)
        {
            GameObject areaGo = new GameObject(area.Name);
            areaGo.transform.parent = homeAssistantConnctor.transform;

            // Confirm area
            Debug.Log("User is in area: " + area.Name);
            homeAssistantAreaManager.CurrentArea = area;

            List<HomeAssistantEntityAreaData> entityAreaData = null;

            // Retrieve all light entities in area
            yield return StartCoroutine(homeAssistantConnctor.RunTemplate<List<HomeAssistantEntityAreaData>>(lightTemplate, states => entityAreaData = states));

            List<HomeAssistantEntity> lights = new List<HomeAssistantEntity>();
            List<Coroutine> coroutines = new List<Coroutine>();
            foreach (var entity in entityAreaData)
            {
                if (entity.area_id == area.Id)
                {
                    GameObject lightGo = Instantiate(lightPrefab, areaGo.transform, false);
                    lightGo.GetComponent<Renderer>().enabled = false;
                    lightGo.name = entity.entity_id;

                    var light = lightGo.GetComponent<HomeAssistantEntity>();

                    Assert.IsNotNull(light);

                    light.EntityId = entity.entity_id;
                    light.IsUpdatingState = false;

                    coroutines.Add(StartCoroutine(
                        light.UpdateStateOnce()
                        ));
                    yield return null;

                    lights.Add(light);
                }
            }

            Debug.Log("Waiting for Update.");

            // Wait for all lights to update their states
            foreach (var coroutine in coroutines)
            {
                yield return coroutine;
            }

            Debug.Log("Entities updated.");

            lights.Sort((HomeAssistantEntity a, HomeAssistantEntity b) => a.Name.CompareTo(b.Name));
            yield return null;

            if (lights.Count == 0)
            {
                Debug.LogError("No lights found.");
            }

            // Turn off all lights
            yield return StartCoroutine(TurnOffAllLights(lights));

            // Walk around room
            brightSpotManager.MarkNewSpotsIrrelevant = true;
            brightSpotDetection.StartDetection();
            Notification.Send("Light Detection", "Please walk around the room slowly and look around.",
                              "Erfassung der Lichter", "Bitte laufe langsam durch den Raum und schaue dich um.");

            yield return new WaitForSeconds(6f);

            brightSpotManager.MarkNewSpotsIrrelevant = false;

            Notification.Send("Light Detection", "Please find and look at the blinking lights.",
                              "Erfassung der Lichter", "Bitte suche und schaue zur der jeweils blinkenden Lampe.");

            // For each light
            foreach (HomeAssistantEntity light in lights)
            {
                bool searching = true;
                int iteration = 0;
                Dictionary<BrightSpot, int> lightCandidates = new Dictionary<BrightSpot, int>();

                while (searching || iteration < minimumCyclesPerLight)
                {
                    iteration++;

                    yield return StartCoroutine(TurnOnLight(light));

                    List<BrightSpot> detectedActiveSpots = brightSpotManager.GetActiveVisibleRelevantSpots();

                    yield return StartCoroutine(TurnOffLight(light));

                    List<BrightSpot> detectedInactiveSpots = brightSpotManager.GetInactiveVisibleRelevantSpots();

                    // Intersection of lists
                    var intersection = detectedActiveSpots.Intersect(detectedInactiveSpots);

                    // Create ranking instead of using intersection every cycle to account for missed detections
                    foreach (BrightSpot spot in intersection)
                    {
                        lightCandidates.TryGetValue(spot, out int numDetections);
                        lightCandidates[spot] = numDetections + 1;
                        Debug.Log("Spot cycle match.");
                    }

                    // Evaluate likelihood
                    if (iteration >= minimumCyclesPerLight)
                    {
                        if (lightCandidates.Count == 0)
                        {
                            Notification.Send("Light Detection", "Please keep your head still and look at the blinking lights.",
                                              "Erfassung der Lichter", "Bitte halte den Kopf ruhig und schaue in Richtung der blinkenden Lampe.");
                        }
                        else
                        {
                            // Found light
                            BrightSpot candidate = null;

                            if (lightCandidates.Count == 1)
                            {
                                candidate = lightCandidates.ElementAt(0).Key;
                            }
                            else if (lightCandidates.Count > 1)
                            {
                                var sortedLightCandidates = lightCandidates.OrderBy(x => x.Value).Reverse();
                                if (sortedLightCandidates.ElementAt(0).Value > 2 * sortedLightCandidates.ElementAt(1).Value)
                                {
                                    // Found light
                                    candidate = sortedLightCandidates.ElementAt(0).Key;
                                }
                            }
                            
                            if (candidate != null)
                            {
                                searching = false;
                                light.transform.position = candidate.transform.position;
                                light.GetComponent<Renderer>().enabled = true;

                                brightSpotManager.DestroySpot(candidate);
                                Debug.Log("Light detected!");
                                break;
                            }
                            
                        }

                    }

                    if (iteration >= maximumCyclesPerLight)
                    {
                        Debug.Log("Aborting: Too many cycles.");
                        searching = false;
                    }


                }


            }

        } else
        {
            brightSpotDetection.StartDetection();
        }



        yield return new WaitForEndOfFrame();

    }

    private IEnumerator TurnOffAllLights(List<HomeAssistantEntity> lights)
    {
        List<Coroutine> coroutines = new List<Coroutine>();
        foreach (var light in lights)
        {
            if (mockHomeAssistantActions)
            {
                Debug.Log("Turning off all lights.");
            }
            else
            {
                coroutines.Add(
                    StartCoroutine(homeAssistantConnctor.CallService(
                    HomeAssistantConnector.Domain.Light, HomeAssistantConnector.Service.TurnOff,
                    new HomeAssistantServiceData { entity_id = light.EntityId })));
            }
        }

        foreach (var coroutine in coroutines)
        {
            yield return coroutine;
        }
    }

    private IEnumerator TurnOnLight(HomeAssistantEntity light)
    {
        yield return StartCoroutine(homeAssistantConnctor.CallService(HomeAssistantConnector.Domain.Light, HomeAssistantConnector.Service.TurnOn, 
            new HomeAssistantServiceDataLightOn { 
                entity_id = light.EntityId,
                brightness = 255,
                color_temp_kelvin = 3000,
            }));
        yield return new WaitForSeconds(turnOnDelay);
    }

    private IEnumerator TurnOffLight(HomeAssistantEntity light)
    {
        yield return StartCoroutine(homeAssistantConnctor.CallService(HomeAssistantConnector.Domain.Light, HomeAssistantConnector.Service.TurnOff,
            new HomeAssistantServiceDataLightOff { 
                entity_id = light.EntityId,
            }));;
        yield return new WaitForSeconds(turnOffDelay);
    }
}
