using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BrightSpotManager : MonoBehaviour
{

    [SerializeField]
    [Range(0f, 3f)]
    [Tooltip("Euclidean distance within which two spots are considered identical.")]
    private float spotDistanceThreshold = .2f;

    [SerializeField]
    private GameObject brightSpotPrefab;

    private List<BrightSpot> brightSpots = new List<BrightSpot>();

    private bool markCurrentSpotsAsIrrelevant;

    public bool MarkNewSpotsIrrelevant { get => markCurrentSpotsAsIrrelevant; set => markCurrentSpotsAsIrrelevant = value; }

    public void AddBrightSpotPosition(Vector3 newPosition)
    {
        // Check light distance to other lights
        bool brightSpotExists = false;
        foreach (BrightSpot brightSpot in brightSpots)
        {
            if (Vector3.Distance(newPosition, brightSpot.Position) <= spotDistanceThreshold)
            {
                // Identical spots
                brightSpot.UpdatePositionTowards(newPosition);
                brightSpot.Relevant = !MarkNewSpotsIrrelevant;
                brightSpotExists = true;
            }
        }

        if (!brightSpotExists)
        {
            GameObject newSpotGameObject = Instantiate(brightSpotPrefab, newPosition, Quaternion.identity, gameObject.transform);

            BrightSpot brightSpot = newSpotGameObject.GetComponent<BrightSpot>();
            if (brightSpot == null)
            {
                brightSpot = newSpotGameObject.AddComponent<BrightSpot>();
            }

            brightSpot.Color = Random.ColorHSV(hueMin: 0, hueMax: 1, saturationMin: .8f, saturationMax: 1, valueMin: .8f, valueMax: 1);
            brightSpot.Relevant = !MarkNewSpotsIrrelevant;

            brightSpots.Add(brightSpot);
        }

    }


    private void Start()
    {
        StartCoroutine(GarbageCollection());
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }

    /// <summary>
    /// Remove close spots. Necessary when one real light created multiple bright spots that move towards each other over time.
    /// </summary>
    private IEnumerator GarbageCollection()
    {
        while (true)
        {
            for (int i = 0; i < brightSpots.Count - 1; i++)
            {

                // Remove spots close to each other
                for (int j = i + 1; j < brightSpots.Count; j++)
                {
                    if (Vector3.Distance(brightSpots[i].Position, brightSpots[j].Position) <= spotDistanceThreshold)
                    {
                        DestroySpot(j);
                        j--;
                    }
                }

                // Remove unconfirmed spots
                if (!brightSpots[i].Confirmed && !brightSpots[i].Alive)
                {
                    DestroySpot(i);
                    i--;
                }
            }

            yield return new WaitForSeconds(2f);
        }
    }

    internal void DestroySpot(BrightSpot brightSpot)
    {
        brightSpots.Remove(brightSpot);
        Destroy(brightSpot.gameObject);
    }

    private void DestroySpot(int j)
    {
        DestroySpot(brightSpots[j]);

    }

    internal List<BrightSpot> GetActiveVisibleRelevantSpots()
    {
        return brightSpots.Where(spot => spot.Alive && spot.Relevant && spot.GetComponent<Renderer>().isVisible).ToList();
    }

    internal List<BrightSpot> GetInactiveVisibleRelevantSpots()
    {
        return brightSpots.Where(spot => !spot.Alive && spot.Relevant && spot.GetComponent<Renderer>().isVisible).ToList();
    }
}
