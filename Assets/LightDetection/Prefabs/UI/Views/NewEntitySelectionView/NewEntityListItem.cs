using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class NewEntityListItem : MonoBehaviour
{
    [SerializeField]
    private TextMeshPro text;
    
    private HomeAssistantEntityData entity;

    public HomeAssistantEntityData Entity { get => entity;
        set
        {
            entity = value;
            string name;
            if (entity.attributes.friendly_name != null && !entity.attributes.friendly_name.Equals(entity.entity_id))
            {
                name = entity.attributes.friendly_name + "\n(" + entity.entity_id + ")";
            }
            else
            {
                name = entity.entity_id;
            }
            text.text = name;
        }
    }

    public void InstantiateEntity()
    {
        if (entity != null)
        {
            HomeAssistantEntityManager.GetInstance().InstantiateEntity(entity);
            UIManager.GetInstance.HideAllViews();
        }
    }
}
