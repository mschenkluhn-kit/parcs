using Microsoft.MixedReality.Toolkit.Utilities;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class NewEntitySelectionView : MonoBehaviour
{
    [SerializeField]
    private GameObject entityList;

    [SerializeField]
    private TextMeshPro loadingText;

    [SerializeField]
    private GameObject entityListItemPrefab;



    private void Start()
    {
        UpdateRecentEntities();
    }

    public void UpdateRecentEntities()
    {
        StartCoroutine(UpdateRecentEntitiesCoroutine());
    }

    private IEnumerator UpdateRecentEntitiesCoroutine()
    {
        loadingText.text = "...";
        loadingText.gameObject.SetActive(true);

        foreach (Transform child in entityList.transform)
        {
            Destroy(child.gameObject);
        }
        entityList.SetActive(false);

        yield return StartCoroutine(HomeAssistantConnector.GetInstance().RunTemplate<List<HomeAssistantEntityData>>(
            new HomeAssistantTemplateData
            {
                template = "{%- set lights = states.light | selectattr('state', 'in', ['on', 'off']) | sort(attribute='last_changed') | list %}[{% for light in lights[-3:] %}{\"entity_id\": \"{{ light.entity_id }}\",\"state\": \"{{ light.state }}\",\"attributes\":{\"friendly_name\": \"{{ light.attributes.friendly_name }}\"}}{% if not loop.last %},{% endif %}{% endfor %}]"
            },
                HandleRecentEntities
            ));
    }

    private void HandleRecentEntities(List<HomeAssistantEntityData> recentEntities)
    {
        loadingText.gameObject.SetActive(false);
        entityList.SetActive(true);

        foreach (HomeAssistantEntityData entity in recentEntities)
        {
            GameObject entityListItem = Instantiate(entityListItemPrefab);
            entityListItem.transform.SetParent(entityList.transform, false);
            entityListItem.GetComponent<NewEntityListItem>().Entity = entity;
        }
        entityList.GetComponent<GridObjectCollection>().UpdateCollection();
    }
}
