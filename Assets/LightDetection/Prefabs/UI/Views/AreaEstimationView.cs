using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;


public class AreaEstimationView : MonoBehaviour
{
    [SerializeField]
    private HomeAssistantAreaManager homeAssistantAreaManager;

    [SerializeField]
    private TextMeshPro areaEstimationText;

    private void Start()
    {
        StartCoroutine(UpdateText());
    }

    private IEnumerator UpdateText()
    {
        yield return StartCoroutine(homeAssistantAreaManager.EstimateCurrentArea());
        areaEstimationText.text = homeAssistantAreaManager.EstimatedCurrentArea.Name;
    }

}
