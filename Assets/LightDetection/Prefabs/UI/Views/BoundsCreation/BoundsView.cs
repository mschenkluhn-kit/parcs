using Microsoft.MixedReality.Toolkit.Utilities.Solvers;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoundsView : MonoBehaviour
{
    [SerializeField]
    private GameObject boundsUI;

    [SerializeField]
    private GameObject newBoundsButton;

    [SerializeField]
    private GameObject cancelCreationButton;

    [SerializeField]
    private GameObject acceptCreationButton;

    [SerializeField]
    private BoundsCreator boundsCreator;

    [SerializeField]
    private GameObject boundsCollection;


    // Start is called before the first frame update
    void Start()
    {
        boundsUI.SetActive(false);
        HomeAssistantEntityManager.GetInstance().SetupModeChangeEvent.AddListener(OnSetupModeChange);
    }

    private void OnEnable()
    {
        ResetUI();
    }

    private void ResetUI()
    {
        newBoundsButton.SetActive(true);
        cancelCreationButton.SetActive(false);
        acceptCreationButton.SetActive(false);

        boundsUI.GetComponent<RadialView>().enabled = true;
    }

    private void OnSetupModeChange()
    {
        if (HomeAssistantEntityManager.GetInstance().SetupModeActive)
        {
            // Activate mesh renderer of each bounds collection child
            foreach (Transform child in boundsCollection.transform)
            {
                child.GetComponent<MeshRenderer>().enabled = true;
            }
        }
        else
        {
            boundsUI.SetActive(false);

            // Deactivate mesh renderer of each bounds collection child
            foreach (Transform child in boundsCollection.transform)
            {
                child.GetComponent<MeshRenderer>().enabled = false;
            }

            boundsCreator.ExitTrackingBoundsMode();
        }
    }

    public void OnNewBoundsButtonClicked()
    {
        boundsUI.SetActive(true);
        newBoundsButton.SetActive(true);
        cancelCreationButton.SetActive(true);
        acceptCreationButton.SetActive(true);

        boundsCreator.StartTrackingBounds();
    }

    public void OnCancelCreationButtonClicked()
    {
        newBoundsButton.SetActive(true);
        cancelCreationButton.SetActive(false);
        acceptCreationButton.SetActive(false);

        boundsCreator.StopTrackingBounds();
    }

    public void OnAcceptCreationButtonClicked()
    {
        newBoundsButton.SetActive(true);
        cancelCreationButton.SetActive(false);
        acceptCreationButton.SetActive(false);

        boundsCreator.AcceptTrackingBounds();
    }

    public void OnOpen()
    {
        ResetUI();
        HomeAssistantEntityManager.GetInstance().SetupModeActive = true;
        boundsUI.SetActive(true);
        boundsCreator.EnterTrackingBoundsMode();
    }

    public void OnClose()
    {
        HomeAssistantEntityManager.GetInstance().SetupModeActive = false;
        boundsCreator.ExitTrackingBoundsMode();
    }

    public void WhileCreatingBounds()
    {
        boundsUI.GetComponent<SolverHandler>().AdditionalOffset = new Vector3(0, -.3f, -.1f);
    }

    public void AfterCreatingBounds()
    {
        boundsUI.GetComponent<SolverHandler>().AdditionalOffset = new Vector3(0, 0, 0);
    }

}
