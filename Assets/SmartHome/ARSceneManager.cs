using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

#if WINDOWS_UWP
using System;
#endif

/// <summary>
/// Functions to next scene and unload current scene, disable, and enable items.
/// </summary>
public class ARSceneManager : MonoBehaviour
{
    [SerializeField]
    [Tooltip("Scene to be loaded when calling LoadNextScene.")]
    private string nextScene;

    [SerializeField]
    [Tooltip("Delay the loading of a new scene.")]
    [Range(0f, 30f)]
    private float loadSceneDelay = 1f;

    [SerializeField]
    [Tooltip("Delay the disabling of a gameobject.")]
    [Range(0f, 50f)]
    private float unloadObjectDelay = .5f;

    [SerializeField]
    [Tooltip("Delay the enabling of a gameobject.")]
    [Range(0f, 50f)]
    private float loadObjectDelay = 1.5f;

    [SerializeField]
    [Tooltip("Time until next scene is automatically loaded. Ignore if 0.")]
    [Range(0f, 600f)]
    private float sceneTimeout = 0f;

    [SerializeField]
    [Tooltip("Disable these GameObjects on startup.")]
    private GameObject[] enableOnStartup;

    [SerializeField]
    [Tooltip("Disable these GameObjects on startup.")]
    private GameObject[] disableOnStartup;

    [SerializeField]
    [Tooltip("Object that should be positioned in accorandce with the reference surface.")]
    private GameObject scenePositionObject;

    [SerializeField]
    [Tooltip("Perform actions on scene loading.")]
    private UnityEvent onSceneLoad;

    private enum LoadOptions
    {
        LoadOnDeviceAndInEditor,
        LoadOnlyOnDevice,
    }

    [SerializeField]
    [Tooltip("Option to only load the scene if running on the HoloLens device.")]
    private LoadOptions LoadOption = LoadOptions.LoadOnlyOnDevice;

    public void Start()
    {
        foreach (GameObject gameObject in disableOnStartup)
        {
            gameObject.SetActive(false);
        }

        foreach (GameObject gameObject1 in enableOnStartup)
        {
            gameObject1.SetActive(true);
        }

        StartCoroutine(AutoloadNextSceneAfterTimeout(sceneTimeout));

        onSceneLoad.Invoke();
    }

    public void LoadNextScene()
    {
        if (nextScene != null)
        {
            LoadScene(nextScene);
        }
    }

    public void LoadScene(string scene)
    {
        if (LoadOption == LoadOptions.LoadOnlyOnDevice && Application.isEditor)
        {
            // Ignore Editor Only
        }
        else
        {
            // Load scene
            StartCoroutine(LoadSceneAsync(scene, loadSceneDelay));
        }
    }

    public void LoadObject(GameObject gameObject)
    {
        StartCoroutine(LoadObjectAsync(gameObject, loadObjectDelay));
    }

    public void UnloadObject(GameObject gameObject)
    {
        StartCoroutine(UnloadObjectAsync(gameObject, unloadObjectDelay));
    }

    public void UnloadObject(string gameObjectName)
    {
        GameObject gameObject = GameObject.Find(gameObjectName);
        if (gameObject != null)
        {
            StartCoroutine(UnloadObjectAsync(gameObject, unloadObjectDelay));
        }
    }

    private IEnumerator LoadSceneAsync(string scene, float delay)
    {
        // Check if scene exists
        if (SceneUtility.GetBuildIndexByScenePath(scene) >= 0)
        {
            yield return new WaitForSeconds(delay);

            string currentScene = SceneManager.GetSceneAt(1).name;

            SceneManager.UnloadSceneAsync(currentScene);
            SceneManager.LoadSceneAsync(scene, LoadSceneMode.Additive);
        }
    }

    private IEnumerator LoadObjectAsync(GameObject gameObject, float delay)
    {
        yield return new WaitForSeconds(delay);

        gameObject.SetActive(true);
    }

    private IEnumerator UnloadObjectAsync(GameObject gameObject, float delay)
    {
        yield return new WaitForSeconds(delay);

        gameObject.SetActive(false);
    }

    private IEnumerator AutoloadNextSceneAfterTimeout(float timeout)
    {
        if (timeout > .1)
        {
            yield return new WaitForSeconds(timeout);

            LoadNextScene();
        }
    }

    public static void RunEyeCalibration()
    {
        LaunchUriAsync("ms-hololenssetup://EyeTracking");
    }

    public static void OpenEyeTrackingPrivacySettings()
    {
        LaunchUriAsync("ms-settings:privacy-eyetracker");
    }

    private static async void LaunchUriAsync(string uri)
    {
#if WINDOWS_UWP
    UnityEngine.WSA.Application.InvokeOnUIThread(async () =>
    {
        bool result = await global::Windows.System.Launcher.LaunchUriAsync(new System.Uri(uri));
        if (!result)
        {
            Debug.LogError("Launching URI failed to launch.");
        }
    }, false);
#else
        Debug.Log("Launching URIs not supported outside of Windows UWP.");
#endif
    }

    public static ARSceneManager GetInstance()
    {
        return GameObject.Find("SceneContent").GetComponent<ARSceneManager>();
    }

}
