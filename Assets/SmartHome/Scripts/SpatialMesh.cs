using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpatialMesh : MonoBehaviour
{
    public bool showInEditorOnly;

    // Start is called before the first frame update
    void Start()
    {
        if (showInEditorOnly && !Application.isEditor)
        {
            gameObject.SetActive(false);
        }
        
    }

}
