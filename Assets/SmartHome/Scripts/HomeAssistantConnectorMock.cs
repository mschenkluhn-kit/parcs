using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

/// <summary>
/// Mocks the Home Assistant Connector to avoid sending commands to the actual instance.
/// States are updated from the actual instance.
/// </summary>
public class HomeAssistantConnectorMock : HomeAssistantConnector
{
    private static HomeAssistantConnector instance;

    private List<HomeAssistantEntity> entities = new List<HomeAssistantEntity>();

    // Call before all Start-Methods
    void OnEnable()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Debug.LogError("Multiple instances of Home Assistant Connector exist!");
        }
    }

    public override IEnumerator UpdateState(HomeAssistantEntity entity, UpdateStateHandler callback)
    {
        yield return new WaitForSeconds(.5f);

        // Return entity if available, else create entity, add it to list, populate it with data and return it
        var state = entities.Find(e => e.EntityId == entity.EntityId).State;
        if (state != null)
        {
            callback(state);
            yield break;
        } else
        {
            state = new HomeAssistantEntityData();
            state.entity_id = entity.EntityId;
            state.state = "off";
            entities.Add(entity);
        }

    }

    public override IEnumerator CallService(HomeAssistantConnector.Domain domain, HomeAssistantConnector.Service service, HomeAssistantServiceData serviceData)
    {
        yield return new WaitForSeconds(.5f);

        var entity = entities.Find(e => e.EntityId == serviceData.entity_id);

        if (domain == Domain.Light)
        {
            if (service == Service.TurnOn)
            {
                entity.State.state = "on";
            }
            else if (service == Service.TurnOff)
            {
                entity.State.state = "off";
            }
            else if (service == Service.Toggle)
            {
                if (entity.State.state == "on")
                {
                    entity.State.state = "off";
                }
                else
                {
                    entity.State.state = "on";
                }
            }

            entity.State.last_changed = DateTime.Now;
            entity.State.last_updated = DateTime.Now;
        }
    }

    public override IEnumerator FireEvent(HomeAssistantConnector.Event eventType, HomeAssistantEventData eventData)
    {
        throw new NotImplementedException();
    }

}
