using Microsoft.MixedReality.Toolkit;
using Microsoft.MixedReality.Toolkit.SpatialAwareness;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExperimentTreatmentAuto : MonoBehaviour
{
    public UIManager uiManager;
    public UIView infoView;

    public AudioClip detectionHintSound;
    public AudioClip allDetectedSound;

    [Range(0f, 1f)]
    public float detectionHintVolume = 1f;
    [Range(0f, 1f)]
    public float allDetectedVolume = 1f;

    [Range(15f, 60f)]
    [SerializeField]
    private float treatmentDuration = 24f;

    public Material spatialPulseMaterial;

    public List<HomeAssistantEntity> entities;

    private AudioSource sharedAudioSource;

    void Start()
    {
        // Make all lamps invisbly tiny
        foreach (HomeAssistantEntity entity in entities)
        {
            entity.transform.localScale = new Vector3(0.001f, 0.001f, 0.001f);
        }

        sharedAudioSource = SharedAudioSource.GetInstance.AudioSource;
    }

    public void OnBegin()
    {
        Debug.Log("Begin");
        uiManager.HideAllViews();
        StartCoroutine(DetectionProcess());
    }

    public void OnComplete()
    {
        sharedAudioSource.PlayOneShot(allDetectedSound, allDetectedVolume);
        Notification.Send("Detection completed", "All devices were successfully detected.", "Erkennung erfolgreich", "Alle Ger�te wurden erfolgreich erkannt.");
        DeactivateSpatialPulse();
    }

    private IEnumerator DetectionProcess()
    {
        yield return StartCoroutine(RunHomeAssistantScript());

        StartCoroutine(TriggerComplete());

        yield return new WaitForSeconds(2f);
        ActivateSpatialPulse();

        yield return new WaitForSeconds(2f);

        // Set entities randomly visible over time
        List<HomeAssistantEntity> entitiesCopy = new List<HomeAssistantEntity>(entities);
        while (entitiesCopy.Count > 0)
        {
            int randomIndex = Random.Range(0, entitiesCopy.Count);
            HomeAssistantEntity entity = entitiesCopy[randomIndex];
            entitiesCopy.RemoveAt(randomIndex);

            SetIndicatorVisible(entity);

            yield return new WaitForSeconds((treatmentDuration - 6f) / 50);
        }
    }

    private IEnumerator RunHomeAssistantScript()
    {
        yield return StartCoroutine(HomeAssistantConnector.GetInstance().CallService(HomeAssistantConnector.Domain.Script, HomeAssistantConnector.Service.AutomatedTreatment, new HomeAssistantServiceData()));
    }

    private IEnumerator TriggerComplete()
    {
        yield return new WaitForSeconds(treatmentDuration);
        OnComplete();
    }

    private void SetIndicatorVisible(HomeAssistantEntity entity)
    {
        entity.transform.localScale = new Vector3(.15f, .15f, .15f);
    }

    private void ActivateSpatialPulse()
    {
        IMixedRealityDataProviderAccess mixedRealityDataProviderAccess = CoreServices.SpatialAwarenessSystem as IMixedRealityDataProviderAccess;
        if (mixedRealityDataProviderAccess != null)
        {
            IReadOnlyList<IMixedRealitySpatialAwarenessMeshObserver> observers = mixedRealityDataProviderAccess.GetDataProviders<IMixedRealitySpatialAwarenessMeshObserver>();
            foreach (IMixedRealitySpatialAwarenessMeshObserver observer in observers)
            {
                observer.DisplayOption = SpatialAwarenessMeshDisplayOptions.Visible;
                observer.VisibleMaterial = spatialPulseMaterial;
            }
        }
    }

    private void DeactivateSpatialPulse()
    {
        IMixedRealityDataProviderAccess mixedRealityDataProviderAccess = CoreServices.SpatialAwarenessSystem as IMixedRealityDataProviderAccess;
        if (mixedRealityDataProviderAccess != null)
        {
            IReadOnlyList<IMixedRealitySpatialAwarenessMeshObserver> observers = mixedRealityDataProviderAccess.GetDataProviders<IMixedRealitySpatialAwarenessMeshObserver>();
            foreach (IMixedRealitySpatialAwarenessMeshObserver observer in observers)
            {
                // Set the mesh to use the occlusion material
                observer.DisplayOption = SpatialAwarenessMeshDisplayOptions.Occlusion;
            }

        }
    }

}
