using Microsoft.MixedReality.Toolkit;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomeAssistantBounds : MonoBehaviour
{
    [SerializeField]
    private string boundsId;

    public string BoundsId { get => boundsId; set => boundsId = value; }

    [SerializeField]
    private string boundsName;

    public string BoundsName { get => boundsName; set => boundsName = value; }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Bounds of No. " + boundsId + " entered.");
        HomeAssistantEntityData entity = BuildEntity();
        entity.state = "on";

        StartCoroutine(HomeAssistantConnector.GetInstance().SendState(entity));
    }

    private void OnTriggerExit(Collider other)
    {
        Debug.Log("Bounds of No. " + boundsId + " exited.");

        HomeAssistantEntityData entity = BuildEntity();
        entity.state = "off";

        StartCoroutine(HomeAssistantConnector.GetInstance().SendState(entity));
    }

    private void OnDestroy()
    {
        Debug.Log("Bounds of No. " + boundsId + " destroyed.");

        HomeAssistantEntityData entity = BuildEntity();
        entity.state = "off";

        StartCoroutine(HomeAssistantConnector.GetInstance().SendState(entity));
    }

    private HomeAssistantEntityData BuildEntity()
    {
        HomeAssistantEntityData entity = new HomeAssistantEntityData();
        entity.entity_id = boundsId;
        entity.attributes = new HomeAssistantEntityData.Attributes();
        entity.attributes.friendly_name = boundsName;
        entity.attributes.icon = "mdi:hololens";
        entity.attributes.device_class = "occupancy";
        
        return entity;
    }

}
