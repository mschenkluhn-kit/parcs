using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadLitecomScene : MonoBehaviour
{
    public int sceneNumber;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(
                HomeAssistantConnector.GetInstance().CallService(HomeAssistantConnector.Domain.ShellCommand, HomeAssistantConnector.Service.LitecomLoadScene, new HomeAssistantServiceDataDaliScene { scene = sceneNumber }));
    }

}
