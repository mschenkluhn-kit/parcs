using Microsoft.MixedReality.Toolkit.Input;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointerSelection : MonoBehaviour
{
    [SerializeField]
    private bool isHandRayPointerActive = false;

    // Start is called before the first frame update
    void Start()
    {
        if (isHandRayPointerActive)
            PointerUtils.SetHandRayPointerBehavior(PointerBehavior.Default);
        else
            PointerUtils.SetHandRayPointerBehavior(PointerBehavior.AlwaysOff);
    }

    private void OnDestroy()
    {
        PointerUtils.SetHandRayPointerBehavior(PointerBehavior.Default);
    }

}
