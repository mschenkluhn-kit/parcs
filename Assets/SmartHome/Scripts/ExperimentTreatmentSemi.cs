using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ExperimentTreatmentSemi : MonoBehaviour
{
    public UIManager uiManager;
    public UIView infoView;
    public UIView resultView;
    public UIView audioView;

    public AudioClip detectionPingSound;
    public AudioClip detectionSuccessSound;
    public AudioClip detectionHintSound;
    public AudioClip allDetectedSound;

    [Range(0f, 1f)]
    public float detectionPingVolume = 1f;
    [Range(0f, 1f)]
    public float detectionSuccessVolume = 1f;
    [Range(0f, 1f)]
    public float detectionHintVolume = 1f;
    [Range(0f, 1f)]
    public float allDetectedVolume = 1f;

    private HomeAssistantEntity currentEntity;
    private int framesCurrentLampIsInFrustum = 0;
    private bool isStepwiseDetectionRunning = false;
    private bool isWaitingForUserInput = false;
    private bool isPingingEntity = false;

    public List<HomeAssistantEntity> entities;
    public List<HomeAssistantEntity> audioEntities;

    private bool areAllLightsDetected = false;

    private AudioSource sharedAudioSource;

    void Start()
    {
        // Make all lamps invisbly tiny
        foreach (HomeAssistantEntity entity in entities)
        {
            entity.transform.localScale = new Vector3(0.001f, 0.001f, 0.001f);
        }

        foreach (HomeAssistantEntity audioEntity in audioEntities)
        {
            audioEntity.transform.localScale = new Vector3(0.001f, 0.001f, 0.001f);
        }

        sharedAudioSource = SharedAudioSource.GetInstance.AudioSource;
    }

    public void OnBeginLampDetection()
    {
        Debug.Log("Begin");
        uiManager.HideAllViews();
        StartCoroutine(DetectLamps());
    }

    public void OnBeginAudioDetection()
    {
        Debug.Log("Begin");
        uiManager.HideAllViews();
        StartCoroutine(DetectAudioSources());
    }

    public void OnProceed()
    {
        Debug.Log("Proceed");
        isWaitingForUserInput = false;
    }

    public void OnSkip()
    {
        Debug.Log("Skip");
        isWaitingForUserInput = false;
        isStepwiseDetectionRunning = false;

        if (areAllLightsDetected)
        {
            StartCoroutine(DetectAllRemainingAudioSources());
        } else
        {
            StartCoroutine(DetectAllRemainingLamps());
        }
        
    }

    private IEnumerator DetectLamps()
    {
        // Dim scene
        yield return StartCoroutine(DimAllLights());
        isStepwiseDetectionRunning = true;
        int numCompletedEntities = 0;

        // For each lamp in list
        foreach (HomeAssistantEntity entity in entities)
        {
            // Set current lamp / reset
            currentEntity = entity;
            framesCurrentLampIsInFrustum = 0;

            Notification.Send("Detection: " + currentEntity.Name, "Searching light entity by using the builtin sensors...", "Erkennung: " + currentEntity.Name, "Suche Lampe indem die eingebauten Sensoren der HoloLens verwendet werden...");

            // Make lamp blink
            StartCoroutine(PingEntity(currentEntity));
            Coroutine soundCoroutine = StartCoroutine(PlayDetectionSound());

            int count = 0;
            while (framesCurrentLampIsInFrustum < 120)
            {
                yield return null;
                count++;

                if (count > 60 * 15)
                {
                    count = -10 * 60;
                    Notification.Send("Hint", "Look at the flashing light.", "Tipp", "Schauen Sie das Licht direkt an, das gerade blinkt.");
                    sharedAudioSource.PlayOneShot(detectionHintSound, detectionHintVolume);
                }
            }

            numCompletedEntities++;
            HomeAssistantEntityData completedEntitiesState = BuildEntity();
            completedEntitiesState.state = "" + numCompletedEntities;
            completedEntitiesState.entity_id = "number.semi_light_entities";
            StartCoroutine(HomeAssistantConnector.GetInstance()
                .SendState(completedEntitiesState));

            // Stop blinking
            isPingingEntity = false;
            SetIndicatorVisible(currentEntity);
            sharedAudioSource.PlayOneShot(detectionSuccessSound, detectionSuccessVolume);
            StopCoroutine(soundCoroutine);
            NotificationManager.GetInstance.CloseCurrentNotification();

            if (entity != entities.Last())
            {
                // Show info that lamp was detected
                uiManager.SetContentView(resultView);
                isWaitingForUserInput = true;

                while (isWaitingForUserInput)
                {
                    yield return null;
                }

                uiManager.HideAllViews();

                if (isStepwiseDetectionRunning == false)
                {
                    yield break;
                }
            }
        }

        areAllLightsDetected = true;
        uiManager.SetContentView(audioView);
        sharedAudioSource.PlayOneShot(detectionHintSound, detectionHintVolume);

    }

    private IEnumerator DetectAudioSources()
    {
        // Dim scene
        yield return StartCoroutine(DimAllLights());
        isStepwiseDetectionRunning = true;
        int numCompletedEntities = 0;

        // For each audiosource in list
        foreach (HomeAssistantEntity entity in audioEntities)
        {
            // Set current audiosource / reset
            currentEntity = entity;
            framesCurrentLampIsInFrustum = 0;

            Notification.Send(currentEntity.Name, "Searching audio sources by using the builtin microphones...", currentEntity.Name, "Suche Lautsprecher indem die eingebauten Sensoren der HoloLens verwendet werden...");

            StartCoroutine(PingEntity(currentEntity));
            Coroutine soundCoroutine = StartCoroutine(PlayDetectionSound());

            // Make lamp blink
            int count = 0;
            while (framesCurrentLampIsInFrustum < 180)
            {
                yield return null;
                count++;

                if (count > 60 * 20)
                {
                    count = -10 * 60;
                    Notification.Send("Hint", "Look at the audio source.", "Tipp", "Schauen Sie den Lautsprecher direkt an, der gerade Musik abspielt.");
                    sharedAudioSource.PlayOneShot(detectionHintSound, detectionHintVolume);
                }
            }

            numCompletedEntities++;
            HomeAssistantEntityData completedEntitiesState = BuildEntity();
            completedEntitiesState.state = "" + numCompletedEntities;
            completedEntitiesState.entity_id = "number.semi_audio_entities";
            StartCoroutine(HomeAssistantConnector.GetInstance()
                .SendState(completedEntitiesState));

            // Stop blinking
            isPingingEntity = false;
            SetIndicatorVisible(currentEntity);
            sharedAudioSource.PlayOneShot(detectionSuccessSound, detectionSuccessVolume);
            StopCoroutine(soundCoroutine);

            NotificationManager.GetInstance.CloseCurrentNotification();

            if (entity != audioEntities.Last())
            {
                // Show info that lamp was detected
                uiManager.SetContentView(resultView);
                isWaitingForUserInput = true;

                while (isWaitingForUserInput)
                {
                    yield return null;
                }

                uiManager.HideAllViews();

                if (isStepwiseDetectionRunning == false)
                {
                    yield break;
                }
            }
        }

        StartCoroutine(OnDetectionSuccessful());

    }



    private void SetIndicatorVisible(HomeAssistantEntity entity)
    {
        entity.transform.localScale = new Vector3(.15f, .15f, .15f);
    }

    private IEnumerator DimAllLights()
    {
        yield return StartCoroutine(HomeAssistantConnector.GetInstance().CallService(HomeAssistantConnector.Domain.ShellCommand, HomeAssistantConnector.Service.LitecomLoadScene, new HomeAssistantServiceDataDaliScene
        {
            scene = 12
        }));
    }

    private IEnumerator OnDetectionSuccessful()
    {
        sharedAudioSource.PlayOneShot(allDetectedSound, allDetectedVolume);

        Notification.Send("Detection: Finished", "All devices were detected.", "Erkennung beendet", "Alle Ger�te wurden erfolgreich erkannt.");
        yield return StartCoroutine(HomeAssistantConnector.GetInstance()
            .CallService(HomeAssistantConnector.Domain.Script, HomeAssistantConnector.Service.stopMusicOnAllDivces, new HomeAssistantServiceData()));

        yield return StartCoroutine(HomeAssistantConnector.GetInstance().CallService(HomeAssistantConnector.Domain.ShellCommand, HomeAssistantConnector.Service.LitecomLoadScene, new HomeAssistantServiceDataDaliScene
        {
            scene = 3
        }));
    }

    private IEnumerator PingEntity(HomeAssistantEntity entity)
    {
        isPingingEntity = true;
        HomeAssistantService service = entity.GetComponent<HomeAssistantService>();

        if (entity.GetComponent<HomeAssistantService>().Domain == HomeAssistantConnector.Domain.ShellCommand)
        {
            int count = 0;
            while (isPingingEntity)
            {
                if (count % 10 == 0)
                {
                    Debug.Log("Blinking " + entity.Name);
                    yield return StartCoroutine(service.RunCoroutine());
                }
                count++;
                yield return new WaitForSeconds(.1f);
            }

            yield return StartCoroutine(DimLight(entity));
        }
        else
        {
            yield return StartCoroutine(service.RunCoroutine());
            yield return new WaitForSeconds(5f);

            while (isPingingEntity)
            {
                yield return null;
            }
            // Call Home Assistant Connector Service to turn off all music
            yield return StartCoroutine(HomeAssistantConnector.GetInstance()
                .CallService(HomeAssistantConnector.Domain.Script, HomeAssistantConnector.Service.stopMusicOnAllDivces, new HomeAssistantServiceData()));

        }
    }

    private IEnumerator PlayDetectionSound()
    {
        while (isPingingEntity)
        {
            sharedAudioSource.PlayOneShot(detectionPingSound, detectionPingVolume);
            yield return new WaitForSeconds(2f);
        }
    }


    private IEnumerator DetectAllRemainingLamps()
    {
        List<Coroutine> parallelDetections = new List<Coroutine>();

        foreach (HomeAssistantEntity entity in entities)
        {
            parallelDetections.Add(StartCoroutine(DetectInParallel(entity)));
            yield return new WaitForSeconds(.1f);
        }

        foreach (Coroutine coroutine in parallelDetections)
        {
            yield return coroutine;
        }

        areAllLightsDetected = true;
        uiManager.SetContentView(audioView);
        sharedAudioSource.PlayOneShot(detectionHintSound, detectionHintVolume);
    }


    private IEnumerator DetectAllRemainingAudioSources()
    {
        List<Coroutine> parallelDetections = new List<Coroutine>();

        foreach (HomeAssistantEntity entity in audioEntities)
        {
            parallelDetections.Add(StartCoroutine(DetectInParallel(entity)));
            yield return new WaitForSeconds(.1f);
        }

        foreach (Coroutine coroutine in parallelDetections)
        {
            yield return coroutine;
        }

        StartCoroutine(OnDetectionSuccessful());
    }

    private IEnumerator DetectInParallel(HomeAssistantEntity entity)
    {
        // Make lamp blink
        Coroutine blinker = StartCoroutine(PingEntity(entity));

        yield return new WaitForSeconds(4f);

        int count = 0;

        while (!entity.GetComponent<Renderer>().isVisible && count < 900)
        {
            count++;
            yield return null;
        }

        StopCoroutine(blinker);
        yield return new WaitForSeconds(.5f);
        
        // If ShellCommand
        if (entity.GetComponent<HomeAssistantService>().Domain == HomeAssistantConnector.Domain.ShellCommand)
        {
            SetIndicatorVisible(entity);
            AudioSource.PlayClipAtPoint(detectionSuccessSound, entity.transform.position, detectionSuccessVolume);
            yield return StartCoroutine(DimLight(entity));
        }
    }

    private IEnumerator DimLight(HomeAssistantEntity entity)
    {
        yield return StartCoroutine(entity.GetComponent<HomeAssistantService>().SetDaliLightIntensity(5));
    }

    private void Update()
    {
        // Check if currentlamp is in view frustum
        if (currentEntity != null)
        {
            // Check Renderer.isVisible
            if (currentEntity.GetComponent<Renderer>().isVisible)
            {
                framesCurrentLampIsInFrustum++;                
            }
            else
            {
                framesCurrentLampIsInFrustum = 0;
            }
            
        }

    }

    private HomeAssistantEntityData BuildEntity()
    {
        HomeAssistantEntityData entity = new HomeAssistantEntityData();
        entity.entity_id = "number.manual_entities";
        entity.attributes = new HomeAssistantEntityData.Attributes();
        entity.attributes.friendly_name = "AR Study - Num Semi Treatment Entities";
        entity.attributes.icon = "mdi:hololens";

        return entity;
    }


}
