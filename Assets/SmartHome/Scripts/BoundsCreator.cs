using Microsoft.MixedReality.Toolkit;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoundsCreator : MonoBehaviour
{
    [SerializeField]
    private GameObject sphere;

    [SerializeField]
    private GameObject boundsCollection;

    [SerializeField]
    private GameObject pointsCollection;

    [SerializeField]
    private GameObject pointPrefab;

    [SerializeField]
    private Material boundsMaterial;

    [SerializeField]
    private float minDistanceBetweenPoints = 0.25f;

    [SerializeField]
    private Color sphereColorInactive = Color.grey;

    [SerializeField]
    private Color sphereColorActive = Color.blue;

    private Vector3 lastPosition;

    private List<Vector3> points = new List<Vector3>();

    private int boundsId = 1;

    private bool trackingBoundsMode = false;
    private bool isCreatingBounds = false;


    void Start()
    {
        sphere.GetComponent<Renderer>().material = new Material(sphere.GetComponent<Renderer>().material);
        sphere.SetActive(false);
    }

    void Update()
    {
        if (isCreatingBounds && (sphere.transform.position - lastPosition).magnitude > minDistanceBetweenPoints)
        {
            lastPosition = sphere.transform.position;
            points.Add(lastPosition);

            // Create a small sphere at the position
            GameObject newPoint = Instantiate(pointPrefab, lastPosition, Quaternion.identity);
            newPoint.transform.parent = pointsCollection.transform;
        }
    }

    public void EnterTrackingBoundsMode()
    {
        if (trackingBoundsMode)
        {
            return;
        }

        trackingBoundsMode = true;

        ResetSphere();
    }

    private void ResetSphere()
    {
        sphere.transform.position = Camera.main.transform.position + Camera.main.transform.forward * 1.0f - Camera.main.transform.up * 0;
        SetSphereColorInactive();
        sphere.SetActive(true);
    }

    public void ExitTrackingBoundsMode()
    {
        if (!trackingBoundsMode)
        {
            return;
        }

        trackingBoundsMode = false;
        sphere.SetActive(false);
    }

    public void StartTrackingBounds()
    {
        isCreatingBounds = true;
        SetSphereColorActive();
    }

    public void StopTrackingBounds()
    {
        isCreatingBounds = false;

        foreach (Transform child in pointsCollection.transform)
        {
            Destroy(child.gameObject);
        }
        points.Clear();

        ResetSphere();
    }

    public void AcceptTrackingBounds()
    {
        Material boundsMat = new Material(boundsMaterial);
        boundsMat.color = Color.HSVToRGB(Random.Range(0.0f, 1.0f), 1f, .5f);
        CreateCollider(CreateMeshGameObject(points, boundsMat));
        StopTrackingBounds();
        ResetSphere();
    }

    private void SetSphereColorActive()
    {
        sphere.GetComponent<Renderer>().material.color = sphereColorActive;
    }

    private void SetSphereColorInactive()
    {
        sphere.GetComponent<Renderer>().material.color = sphereColorInactive;
    }


    private GameObject CreateMeshGameObject(List<Vector3> points, Material material)
    {
        GameObject boundsGo = new GameObject();
        boundsGo.AddComponent<MeshRenderer>();
        boundsGo.AddComponent<MeshFilter>();
        boundsGo.GetComponent<MeshFilter>().mesh = CreateMesh(points);
        boundsGo.GetComponent<MeshRenderer>().material = material;
        boundsGo.transform.parent = boundsCollection.transform;

        HomeAssistantBounds bounds = boundsGo.AddComponent<HomeAssistantBounds>();
        bounds.BoundsId = "binary_sensor.hololens_area_" + boundsId;
        bounds.BoundsName = "HoloLens Area " + boundsId;

        boundsId++;
        return boundsGo;
    }

    // Create a mesh from a list of points
    private Mesh CreateMesh(List<Vector3> points)
    {
        Mesh mesh = new Mesh();
        Vector3[] vertices = new Vector3[points.Count];

        for (int i = 0; i < points.Count; i++)
        {
            vertices[i] = new Vector3(points[i].x, -1f, points[i].z);
        }

        mesh.vertices = vertices;

        // Create triangles
        int[] triangles = new int[(points.Count - 2) * 3];
        for (int i = 0, j = 1; j < points.Count - 1; i += 3, j++)
        {
            triangles[i] = 0;
            triangles[i + 1] = j;
            triangles[i + 2] = j + 1;
        }
        mesh.triangles = triangles;

        // Create normals
        Vector3[] normals = new Vector3[vertices.Length];
        for (int i = 0; i < normals.Length; i++)
        {
            normals[i] = Vector3.up;
        }
        mesh.normals = normals;

        // mesh.RecalculateNormals();
        mesh.RecalculateBounds();
        mesh.RecalculateUVDistributionMetrics();

        return mesh;
    }

    // Create collider for a mesh
    private void CreateCollider(GameObject gameObject)
    {
        MeshCollider mc = gameObject.AddComponent<MeshCollider>();
        mc.sharedMesh = gameObject.GetComponent<MeshFilter>().mesh;
        mc.convex = true;
        mc.isTrigger = true;
    }
}
