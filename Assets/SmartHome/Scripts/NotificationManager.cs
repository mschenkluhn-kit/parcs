using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NotificationManager : MonoBehaviour
{

    [SerializeField]
    private GameObject notificationPrefab;

    private static NotificationManager instance;

    public static NotificationManager GetInstance => instance;

    private Notification currentNotification;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        } else
        {
            Debug.LogWarning("Multiple instances of NotificationManager found.");
        }
    }

    public void Send(string englishTitle, string englishMessage, string germanTitle, string germanMessage)
    {
        CloseCurrentNotification();

        var notification = Instantiate(notificationPrefab);
        currentNotification = notification.GetComponent<Notification>();
        if (DisplayLanguageSelector.GetInstance.SelectedLanguage == DisplayLanguageSelector.Language.English)
        {
            currentNotification.Set(englishTitle, englishMessage);
        } else
        {
            currentNotification.Set(germanTitle, germanMessage);
        }
        notification.transform.SetParent(transform);
        
        
    }

    public void CloseCurrentNotification()
    {
        if (currentNotification != null)
        {
            currentNotification.FadeOut(0);
        }
    }

}
