using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;
using static HomeAssistantConnector;
using static UnityEngine.EventSystems.EventTrigger;

public class HomeAssistantConnector : MonoBehaviour
{

    private string serverUri;
    private string bearerToken;

    [Header("Credentials are stored in the CredentialManager locally.")]
    protected HomeAssistantCredentials credentials;

    [SerializeField]
    private bool storeIpLocally = true;

    [SerializeField]
    private bool useIpFromLocalStorage = true;

    protected static HomeAssistantConnector instance;

    // Call before all Start-Methods
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Debug.LogError("Multiple instances of Home Assistant Connector exist!");
        }
    }


    protected virtual async void OnEnable()
    {
        credentials = await LocalCredentialStore.Load();

        if (credentials == null)
        {
            Debug.LogError("No credentials stored locally! Please update the credential file in Documents/AR Smart Home");
            LocalCredentialStore.Save(new HomeAssistantCredentials { homeAssistantServerUrl = "", homeAssistantApiKey = "" });
            FindObjectOfType<HomeAssistantReachable>().enabled = false;
            StartCoroutine(SendNoAccessFileNotification());
        }
        else
        {
            serverUri = credentials.homeAssistantServerUrl;
            bearerToken = credentials.homeAssistantApiKey;
        }
    }

    private IEnumerator SendNoAccessFileNotification()
    {
        yield return new WaitForSeconds(1);
        Notification.Send("No Access File", "No connection data stored locally! Please update the credential file in the AR Smart Home folder.",
                          "Zugangsdatei fehlt", "Konnte keine Verbindungsdaten finden! Bitten update die Zugangsdatei im lokalen AR Smart Home Ordner.");
    }

    public delegate void AllStatesHandler(List<HomeAssistantEntityData> states);

    public virtual IEnumerator GetAllStates(AllStatesHandler callback)
    {
        using UnityWebRequest request = UnityWebRequest.Get(serverUri + "/api/states");
        request.timeout = 1;
        request.SetRequestHeader("Content-Type", "application/json");
        request.SetRequestHeader("Authorization", "Bearer " + bearerToken);
        yield return request.SendWebRequest();

        switch (request.result)
        {
            case UnityWebRequest.Result.ConnectionError:
            case UnityWebRequest.Result.DataProcessingError:
                Debug.LogError("Error: " + request.error);
                break;
            case UnityWebRequest.Result.ProtocolError:
                Debug.LogError("HTTP Error: " + request.error);
                break;
            case UnityWebRequest.Result.Success:
                callback(JsonConvert.DeserializeObject<List<HomeAssistantEntityData>>(request.downloadHandler.text));
                break;
        }

    }

    public delegate void ServerReachableHandler(bool health);

    // Method that returns if the server is reachable
    public virtual IEnumerator IsServerReachable(ServerReachableHandler callback)
    {
        using UnityWebRequest request = UnityWebRequest.Get(serverUri + "/api/");
        request.timeout = 1;
        request.SetRequestHeader("Content-Type", "application/json");
        request.SetRequestHeader("Authorization", "Bearer " + bearerToken);
        yield return request.SendWebRequest();

        switch (request.result)
        {
            case UnityWebRequest.Result.ConnectionError:
            case UnityWebRequest.Result.DataProcessingError:
            case UnityWebRequest.Result.ProtocolError:
                callback(false);
                break;
            case UnityWebRequest.Result.Success:
                callback(true);
                break;
        }

    }

    public delegate void HomeAssistantConfigHandler(HomeAssistantConfigData configData);

    // Method that returns if the server is reachable
    public virtual IEnumerator GetConfig(HomeAssistantConfigHandler callback)
    {
        using UnityWebRequest request = UnityWebRequest.Get(serverUri + "/api/config");
        request.timeout = 1;
        request.SetRequestHeader("Content-Type", "application/json");
        request.SetRequestHeader("Authorization", "Bearer " + bearerToken);
        yield return request.SendWebRequest();

        switch (request.result)
        {
            case UnityWebRequest.Result.ConnectionError:
            case UnityWebRequest.Result.DataProcessingError:
            case UnityWebRequest.Result.ProtocolError:
                callback(null);
                break;
            case UnityWebRequest.Result.Success:
                callback(JsonConvert.DeserializeObject<HomeAssistantConfigData>(request.downloadHandler.text));
                break;
        }

    }

    public delegate void UpdateStateHandler(HomeAssistantEntityData state);

    public virtual IEnumerator UpdateState(HomeAssistantEntity entity, UpdateStateHandler callback)
    {
        using UnityWebRequest request = UnityWebRequest.Get(serverUri + "/api/states/" + entity.EntityId);
        request.timeout = 1;
        request.SetRequestHeader("Content-Type", "application/json");
        request.SetRequestHeader("Authorization", "Bearer " + bearerToken);
        yield return request.SendWebRequest();

        switch (request.result)
        {
            case UnityWebRequest.Result.ConnectionError:
            case UnityWebRequest.Result.DataProcessingError:
                Debug.LogError("Error: " + request.error + " (Entity: " + entity.EntityId + ")");
                break;
            case UnityWebRequest.Result.ProtocolError:
                Debug.LogError("HTTP Error: " + request.error + " (Entity: " + entity.EntityId + ")");
                break;
            case UnityWebRequest.Result.Success:
                callback(JsonConvert.DeserializeObject<HomeAssistantEntityData>(request.downloadHandler.text));
                break;
        }

    }

    public string msgs = "";

    public void DebugHA(string msg)
    {
        HomeAssistantEntityData homeAssistantEntityData = new HomeAssistantEntityData();
        homeAssistantEntityData.entity_id = "sensor.hl_debug";
        //homeAssistantEntityData.state = msgs + "\n" + msg;
        homeAssistantEntityData.state = msg;
        msgs = homeAssistantEntityData.state;
        homeAssistantEntityData.attributes.friendly_name = "HoloLens Debug";
        //StartCoroutine(SendState(homeAssistantEntityData));
    }

    public virtual IEnumerator SendState(HomeAssistantEntityData entity)
    {
        byte[] payload = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(entity));

        var request = UnityWebRequest.Post(serverUri + "/api/states/" + entity.entity_id, "");
        request.timeout = 1;
        request.SetRequestHeader("Content-Type", "application/json");
        request.SetRequestHeader("Authorization", "Bearer " + bearerToken);

        request.uploadHandler = new UploadHandlerRaw(payload);

        yield return request.SendWebRequest();

        if (request.result == UnityWebRequest.Result.ConnectionError)
        {
            Debug.LogWarning("Connection error.");
            yield return new WaitForSeconds(5);
        }

        // Debug.Log(request.downloadHandler.text);

        yield return null;
    }

    public virtual IEnumerator CallService(HomeAssistantConnector.Domain domain, HomeAssistantConnector.Service service, HomeAssistantServiceData serviceData)
    {
        byte[] payload = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(serviceData));

        Debug.Log("Call Service: " + serverUri + "/api/services/" + DomainAsString(domain) + "/" + ServiceAsString(service) + " with payload: " + JsonConvert.SerializeObject(serviceData));

        //string payload = JsonConvert.SerializeObject(serviceData);

        var request = UnityWebRequest.Post(serverUri + "/api/services/" + DomainAsString(domain) + "/" + ServiceAsString(service), "");
        request.timeout = 1;
        request.SetRequestHeader("Content-Type", "application/json");
        request.SetRequestHeader("Authorization", "Bearer " + bearerToken);

        //Debug.Log("Call Service: " + serverUri + "/api/services/" + DomainAsString(domain) + "/" + ServiceAsString(service));

        request.uploadHandler = new UploadHandlerRaw(payload);

        yield return request.SendWebRequest();

        if (request.result == UnityWebRequest.Result.ConnectionError)
        {
            Debug.LogWarning("Connection error.");
            yield return new WaitForSeconds(5);
        }

        yield return null;
    }

    public virtual IEnumerator FireEvent(HomeAssistantConnector.Event eventType, HomeAssistantEventData eventData)
    {
        byte[] payload = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(eventData));

        //string payload = JsonConvert.SerializeObject(serviceData);

        var request = UnityWebRequest.Post(serverUri + "/api/events/" + EventAsString(eventType), "");
        request.timeout = 1;
        request.SetRequestHeader("Content-Type", "application/json");
        request.SetRequestHeader("Authorization", "Bearer " + bearerToken);

        request.uploadHandler = new UploadHandlerRaw(payload);

        yield return request.SendWebRequest();

        if (request.result == UnityWebRequest.Result.ConnectionError)
        {
            Debug.LogWarning("Connection error.");
            yield return new WaitForSeconds(5);
        }

        yield return null;
    }

    public delegate void TemplateResultHandler<T>(T result);

    public virtual IEnumerator RunTemplate<T>(HomeAssistantTemplateData template, TemplateResultHandler<T> callback)
    {
        byte[] payload = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(template));

        //string payload = JsonConvert.SerializeObject(serviceData);

        var request = UnityWebRequest.Post(serverUri + "/api/template", "");
        request.timeout = 1;
        request.SetRequestHeader("Content-Type", "application/json");
        request.SetRequestHeader("Authorization", "Bearer " + bearerToken);

        request.uploadHandler = new UploadHandlerRaw(payload);

        yield return request.SendWebRequest();

        switch (request.result)
        {
            case UnityWebRequest.Result.Success:
                callback(JsonConvert.DeserializeObject<T>(request.downloadHandler.text));
                break;
            case UnityWebRequest.Result.ConnectionError:
            case UnityWebRequest.Result.ProtocolError:
            case UnityWebRequest.Result.DataProcessingError:
                Debug.LogError("Error: " + request.error);
                yield return new WaitForSeconds(5);
                break;
            default:
                break;
        }

        yield return null;
    }

    public static HomeAssistantConnector GetInstance() => instance;

    public static string DomainAsString(HomeAssistantConnector.Domain domain)
    {
        string s = "";

        switch (domain)
        {
            case Domain.Light:
                s = "light";
                break;
            case Domain.Tado:
                s = "tado";
                break;
            case Domain.ShellCommand:
                s = "shell_command";
                break;
            case Domain.Script:
                s = "script";
                break;
        }

        return s;
    }

    public static string ServiceAsString(HomeAssistantConnector.Service service)
    {
        string s = "";

        switch (service)
        {
            case Service.TurnOn:
                s = "turn_on";
                break;
            case Service.TurnOff:
                s = "turn_off";
                break;
            case Service.Toggle:
                s = "toggle";
                break;
            case Service.SetClimateTimer:
                s = "set_climate_timer";
                break;
            case Service.LitecomSetLight:
                s = "litecom_set_light";
                break;
            case Service.LitecomSetBlinds:
                s = "litecom_set_blinds";
                break;
            case Service.LitecomLoadScene:
                s = "litecom_load_scene";
                break;
            case Service.playMusicOnDevice:
                s = "ar_study_play_music_on_device";
                break;
            case Service.stopMusicOnAllDivces:
                s = "ar_study_stop_music_on_all_devices";
                break;
            case Service.AutomatedTreatment:
                s = "ar_study_configure_everything";
                break;
            default:
                break;
        }

        return s;
    }

    public static string EventAsString(HomeAssistantConnector.Event eventType)
    {
        string s = "";

        switch (eventType)
        {
            case Event.Presence:
                s = "ar_presence_event";
                break;
            default:
                break;
        }

        return s;
    }


    public enum Domain
    {
        Light = 0,
        Tado = 1,
        ShellCommand = 2,
        Script = 3
    }

    public enum Service
    {
        TurnOn = 0,
        TurnOff = 1,
        Toggle = 2,
        SetClimateTimer = 3,
        LitecomSetLight = 4,
        LitecomLoadScene = 5,
        playMusicOnDevice = 6,
        stopMusicOnAllDivces = 7,
        AutomatedTreatment = 8,
        LitecomSetBlinds = 9
    }

    public enum Event
    {
        Presence = 0
    }
}
