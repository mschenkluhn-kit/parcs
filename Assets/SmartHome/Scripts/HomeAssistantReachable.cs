using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class HomeAssistantReachable : MonoBehaviour
{
    [SerializeField]
    private bool sendNotificationOnStatusChange = true;

    [SerializeField]
    private UnityEvent homeAssistantNowReachable;

    [SerializeField]
    private UnityEvent homeAssistantNotReachableAnymore;

    private bool currentState = true;
    private int currentStateCounter = 0;

    public bool Connected { get => currentState; private set => currentState = value; }


    void Start()
    {
        StartCoroutine(CheckServerReachable());
    }

    private IEnumerator CheckServerReachable()
    {
        yield return new WaitForSeconds(.5f);

        while (true)
        {
            // Check if server is reachable
            yield return StartCoroutine(HomeAssistantConnector.GetInstance().IsServerReachable(HandleServerReachableUpdate));
            yield return new WaitForSeconds(2);
        }
    }

    private void HandleServerReachableUpdate(bool reachable)
    {
        if (reachable != currentState || currentStateCounter == 0)
        {
            // Trigger event
            if (reachable)
            {
                homeAssistantNowReachable.Invoke();
                StartCoroutine(HomeAssistantConnector.GetInstance().GetConfig(HandleServerReachableNow));
            }
            else
            {
                homeAssistantNotReachableAnymore.Invoke();
                Notification.Send("Home Assistant", "Could not establish connection to server.",
                                  "Home Assistant", "Serververbindung konnte nicht hergestellt werden.");
            }

            currentState = reachable;
            currentStateCounter = 1;
        } else
        {
            currentStateCounter++;
        }
    }

    private void HandleServerReachableNow(HomeAssistantConfigData configData)
    {
        if (configData != null)
        {
            Notification.Send("Home Assistant",
            String.Format("Server connection established to instance \"{0}\" running HA {1} with {2} components.",
            configData.location_name, configData.version, configData.components.Count),
            "Home Assistant",
            String.Format("Serververbindung zu \"{0}\" hergestellt mit HA {1} ({2} Elemente).",
            configData.location_name, configData.version, configData.components.Count));
        } else
        {
            Debug.LogWarning("Could not read config data from server.");
        }
        
    }

}
