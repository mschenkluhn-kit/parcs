using UnityEngine;

public class RoomSelector : MonoBehaviour
{

    [SerializeField]
    private LoadOptions loadOptions;

    [SerializeField]
    private bool showEntities = false;

    [SerializeField]
    private bool showWorldLockingPins = false;

    [SerializeField]
    private bool showMesh = false;

    [SerializeField]
    private bool showBounds = false;

    // Start is called before the first frame update
    void Start()
    {
        foreach (Transform child in transform)
        {
            child.gameObject.SetActive(false);
        }
        GameObject room = transform.Find(loadOptions.ToString()).gameObject;
        room.SetActive(true);

        room.transform.Find("Entities").gameObject.SetActive(showEntities);
        room.transform.Find("Orienters").gameObject.SetActive(showWorldLockingPins);
        room.transform.Find("Mesh").gameObject.SetActive(showMesh);
        Transform areas = room.transform.Find("Areas");
        foreach (Transform area in areas)
        {
            area.gameObject.GetComponent<Renderer>().enabled = showBounds;
        }            
    }

    private enum LoadOptions
    {
        IISM,
        Home
    }
}
