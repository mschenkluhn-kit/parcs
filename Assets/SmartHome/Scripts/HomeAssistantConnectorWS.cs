using Newtonsoft.Json;
using System.Collections;
using System.Net.WebSockets;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class HomeAssistantConnectorWS : HomeAssistantConnector
{
    protected static new HomeAssistantConnectorWS instance;
    public static new HomeAssistantConnectorWS GetInstance() => instance;

    public UnityEvent connectionEstablished;
    public UnityEvent connectionLost;

    private HomeAssistantWebsocket homeAssistantWebsocket;

    private string notification;

    // Call before all Start-Methods
    private void Awake()
    {
        if (HomeAssistantConnector.instance == null && instance == null)
        {
            HomeAssistantConnector.instance = this;
            instance = this;
        }
        else
        {
            Debug.LogError("Multiple instances of Home Assistant Connector exist!");
        }

    }

    private void Update()
    {
        if (notification != null && notification != "")
        {
            notification = "";
        }
    }

    protected override void OnEnable()
    {
        base.OnEnable();

        WebSocketConnect();
    }

    private async void WebSocketConnect()
    {
        homeAssistantWebsocket = new HomeAssistantWebsocket();
        await homeAssistantWebsocket.ConnectAsync(base.credentials);
    }

    public async void Subscribe(HomeAssistantSubscriptionData body, HomeAssistantWebsocket.SubscriptionResponseHandler responseHandler)
    {
        await Task.Run(async () =>
        {
            while (homeAssistantWebsocket.ConnectionState != WebSocketState.Open)
            {
                await Task.Delay(100);
            }

            await homeAssistantWebsocket.Subscribe(body, responseHandler);
        });
    }
}
