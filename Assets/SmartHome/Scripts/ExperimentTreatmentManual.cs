using Microsoft.MixedReality.Toolkit.Utilities.Solvers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExperimentTreatmentManual : MonoBehaviour
{
    public UIManager uiManager;
    public UIView infoView;

    public AudioClip detectionPingSound;
    public AudioClip detectionSuccessSound;
    public AudioClip detectionHintSound;
    public AudioClip allDetectedSound;

    [Range(0f, 1f)]
    public float detectionPingVolume = 1f;
    [Range(0f, 1f)]
    public float detectionSuccessVolume = 1f;
    [Range(0f, 1f)]
    public float detectionHintVolume = 1f;
    [Range(0f, 1f)]
    public float allDetectedVolume = 1f;

    private HomeAssistantEntity currentEntity;
    private bool isWaitingForUserInput = false;

    public List<HomeAssistantEntity> entities;

    private AudioSource sharedAudioSource;

    private bool started = false;
    private HandConstraintPalmUp handMenu;

    void Start()
    {
        // Make all lamps invisbly tiny
        foreach (HomeAssistantEntity entity in entities)
        {
            entity.transform.localScale = new Vector3(0.001f, 0.001f, 0.001f);
        }

        sharedAudioSource = SharedAudioSource.GetInstance.AudioSource;

        handMenu = GameObject.Find("HandMenu").GetComponent<HandConstraintPalmUp>();
    }

    public void OnBegin()
    {
        started = true;
        Debug.Log("Begin");
        uiManager.HideAllViews();
        StartCoroutine(MarkEntities());
    }

    public void OnProceed()
    {
        if (started)
        {
            Debug.Log("Proceed");
            isWaitingForUserInput = false;
            handMenu.enabled = false;
            StartCoroutine(ReenableHandMenu());
        }
    }

    private IEnumerator ReenableHandMenu()
    {
        yield return new WaitForSeconds(3f);
        handMenu.enabled = true;
    }

    private IEnumerator MarkEntities()
    {
        // Dim scene
        yield return StartCoroutine(DimAllLights());

        int numCompletedEntities = 0;

        // For each lamp in list
        foreach (HomeAssistantEntity entity in entities)
        {
            // Set current lamp / reset
            currentEntity = entity;

            Notification.Send(currentEntity.Name, "Please look for the highlighted entity and mark it with one of the spheres.", currentEntity.Name, "Bitte suchen Sie nach dem Ger�t, das auf sich aufmerksam macht und markieren es mit einer der Kugeln auf dem Boden.");

            // Make lamp blink
            Coroutine pingCoroutine = StartCoroutine(PingEntity(currentEntity));

            int count = 0;
            isWaitingForUserInput = true;
            while (isWaitingForUserInput)
            {
                if (count > 60 * 120)
                {
                    count = -20 * 60;
                    Notification.Send("Hint", "When you have marked the entity, use your hand menu to proceed.", "Tipp", "Nachdem Sie das Ger�t mit der Kugel markiert haben, k�nnen Sie �ber das Handmen� fortfahren.");
                    sharedAudioSource.PlayOneShot(detectionHintSound, detectionHintVolume);
                }
                count++;

                yield return null;
            }

            numCompletedEntities++;
            HomeAssistantEntityData completedEntitiesState = BuildEntity();
            completedEntitiesState.state = "" + numCompletedEntities;
            StartCoroutine(HomeAssistantConnector.GetInstance()
                .SendState(completedEntitiesState));

            // Stop blinking
            Debug.Log("Stop blinking " + currentEntity.Name);
            StartCoroutine(DimLight(currentEntity));
            StopCoroutine(pingCoroutine);
            sharedAudioSource.PlayOneShot(detectionSuccessSound, detectionSuccessVolume);
            yield return StartCoroutine(HomeAssistantConnector.GetInstance()
                .CallService(HomeAssistantConnector.Domain.Script, HomeAssistantConnector.Service.stopMusicOnAllDivces, new HomeAssistantServiceData()));
            NotificationManager.GetInstance.CloseCurrentNotification();
        }

        StartCoroutine(OnMarkingSuccessful());
    }

    private IEnumerator DimAllLights()
    {
        Debug.Log("Dim all lights");
        yield return StartCoroutine(HomeAssistantConnector.GetInstance().CallService(HomeAssistantConnector.Domain.ShellCommand, HomeAssistantConnector.Service.LitecomLoadScene, new HomeAssistantServiceDataDaliScene
        {
            scene = 12
        }));
    }

    private IEnumerator OnMarkingSuccessful()
    {
        Debug.Log("OnMarkingSuccessful");
        sharedAudioSource.PlayOneShot(allDetectedSound, allDetectedVolume);
        handMenu.enabled = false;

        Notification.Send("Finished", "You have marked all devices.", "Erfolg", "Sie haben alle Ger�te erfolgreich markiert.");
        yield return StartCoroutine(HomeAssistantConnector.GetInstance()
            .CallService(HomeAssistantConnector.Domain.Script, HomeAssistantConnector.Service.stopMusicOnAllDivces, new HomeAssistantServiceData()));

        yield return StartCoroutine(HomeAssistantConnector.GetInstance().CallService(HomeAssistantConnector.Domain.ShellCommand, HomeAssistantConnector.Service.LitecomLoadScene, new HomeAssistantServiceDataDaliScene
        {
            scene = 3
        }));
    }

    private IEnumerator PingEntity(HomeAssistantEntity entity)
    {
        HomeAssistantService service = entity.GetComponent<HomeAssistantService>();

        Debug.Log("Ping " + entity.Name);

        if (entity.GetComponent<HomeAssistantService>().Domain == HomeAssistantConnector.Domain.ShellCommand)
        {
            while (true)
            {
                Debug.Log("Blinking " + entity.Name);
                yield return StartCoroutine(service.RunCoroutine());
                yield return new WaitForSeconds(1f);
            }
        }
        else
        {
            yield return StartCoroutine(service.RunCoroutine());
        }
    }

    private IEnumerator DimLight(HomeAssistantEntity entity)
    {
        yield return StartCoroutine(entity.GetComponent<HomeAssistantService>().SetDaliLightIntensity(5));
    }

    private HomeAssistantEntityData BuildEntity()
    {
        HomeAssistantEntityData entity = new HomeAssistantEntityData();
        entity.entity_id = "number.manual_entities";
        entity.attributes = new HomeAssistantEntityData.Attributes();
        entity.attributes.friendly_name = "AR Study - Num Manual Treatment Entities";
        entity.attributes.icon = "mdi:hololens";

        return entity;
    }

}
