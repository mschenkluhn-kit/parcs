using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SharedAudioSource : MonoBehaviour
{
    [SerializeField]
    private bool placeInFrontOfCamera = false;

    private static SharedAudioSource instance;

    public static SharedAudioSource GetInstance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<SharedAudioSource>();
            }
            return instance;
        }
    }

    private AudioSource audioSource;

    public AudioSource AudioSource { get => audioSource; set => audioSource = value; }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }

        audioSource = GetComponent<AudioSource>();
    }

    private void OnDestroy()
    {
        if (instance == this)
        {
            instance = null;
        }
    }

    public void PlaySound(AudioClip clip, float volumeScale)
    {
        audioSource.PlayOneShot(clip, volumeScale);
    }


    void Update()
    {
        if (placeInFrontOfCamera)
        {
            transform.position = Camera.main.transform.position + Camera.main.transform.forward * 1;
        }
    }

}
