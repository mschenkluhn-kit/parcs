using System;
using System.Threading.Tasks;
using Newtonsoft.Json;
using UnityEngine;
using System.Collections;

#if UNITY_WSA && !UNITY_EDITOR
using Windows.Storage;
#else
using System.IO;
#endif

public class LocalCredentialStore
{
    public static async void Save(HomeAssistantCredentials credentials)
    {
        string credentialsJson = JsonConvert.SerializeObject(credentials);

#if UNITY_WSA && !UNITY_EDITOR

        StorageFolder fLocal = ApplicationData.Current.LocalFolder;

        StorageFile file = await fLocal.CreateFileAsync("Home Assistant Credentials.txt", CreationCollisionOption.OpenIfExists);

        var f = FileIO.WriteTextAsync(file, credentialsJson);

        await f;
#else

        // Win32
        var documents = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

        // Create folder if it doesnt exist
        Directory.CreateDirectory(Path.Combine(documents, "AR Smart Home"));

        var path = Path.Combine(documents, "AR Smart Home", "Home Assistant Credentials.txt");

        // Save to file
        File.WriteAllText(path, credentialsJson);
#endif
    }

    public static async Task<bool> DoesLocalFileExist()
    {
        bool exists = false;

#if UNITY_WSA && !UNITY_EDITOR
            try
            {
                StorageFolder fLocal = ApplicationData.Current.LocalFolder;

                StorageFile file = await fLocal.GetFileAsync("Home Assistant Credentials.txt");
                if (file != null) {
                    exists = true;
                }
            } catch {
            }
#else

        // Win32
        var documents = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

        // Create folder if it doesnt exist
        Directory.CreateDirectory(Path.Combine(documents, "AR Smart Home"));

        var path = Path.Combine(documents, "AR Smart Home", "Home Assistant Credentials.txt");

        // Read from file
        if (File.Exists(path))
        {
            exists = true;
        }
#endif
        return exists;
    }

    public static async Task<HomeAssistantCredentials> Load()
    {
        string credentialsJson = null;

#if UNITY_WSA && !UNITY_EDITOR
            try
            {
                StorageFolder fLocal = ApplicationData.Current.LocalFolder;

                StorageFile file = await fLocal.GetFileAsync("Home Assistant Credentials.txt");
                if (file != null) {
                    credentialsJson = await FileIO.ReadTextAsync(file);
                }
            } catch {
            }
#else

        // Win32
        var documents = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

        // Create folder if it doesnt exist
        Directory.CreateDirectory(Path.Combine(documents, "AR Smart Home"));

        var path = Path.Combine(documents, "AR Smart Home", "Home Assistant Credentials.txt");

        // Read from file
        if (File.Exists(path))
        {
            credentialsJson = File.ReadAllText(path);
        }
#endif
        if (credentialsJson == null || credentialsJson.Length == 0)
        {
            return null;
        }
        else
        {
            return JsonConvert.DeserializeObject<HomeAssistantCredentials>(credentialsJson);
        }
    }

}
