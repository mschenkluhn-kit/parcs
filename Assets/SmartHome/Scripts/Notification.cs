using Microsoft.MixedReality.Toolkit.Utilities.Solvers;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Playables;

public class Notification : MonoBehaviour
{
    [SerializeField]
    private TextMeshPro title;

    [SerializeField]
    private TextMeshPro message;

    [SerializeField]
    [Range(0, 10)]
    private float notificationDuration = 5f;

    [SerializeField]
    private float notificationDistanceToUser = .9f;

    public static void Send(string englishTitle, string englishMessage, string germanTitle, string germanMessage)
    {
        NotificationManager.GetInstance.Send(englishTitle, englishMessage, germanTitle, germanMessage);
    }

    public void Set(string title, string message)
    {
        this.title.text = title;
        this.message.text = message;

        transform.position = Camera.main.transform.position + Camera.main.transform.forward * notificationDistanceToUser;
        transform.LookAt(Camera.main.transform.position);
        transform.Rotate(0, 180, 0);


        StartCoroutine(FadeInCoroutine());

        StartCoroutine(FadeOutCoroutine(notificationDuration));
    }

    public void Set(string title, string message, float notificationDuration)
    {
        this.notificationDuration = notificationDuration;
        Set(title, message);
    }

    public void Set(string title, string message, float notificationDuration, float notificationDistanceToUser)
    {
        this.notificationDistanceToUser = notificationDistanceToUser;
        Set(title, message, notificationDuration);
    }

    private IEnumerator FadeInCoroutine()
    {
        transform.localScale = Vector3.one * .8f;

        while (transform.localScale.x < 1)
        {
            transform.localScale += Vector3.one * Time.deltaTime;
            yield return null;
        }
        transform.localScale = Vector3.one;
    }

    public void FadeOut(float delay = 0)
    {
        StartCoroutine(FadeOutCoroutine(delay));
    }

    private IEnumerator FadeOutCoroutine(float delay)
    {
        yield return new WaitForSeconds(delay);

        // Deactivate possible solver scripts that interfer with the animation
        var solver = GetComponent<SolverHandler>();
        if (solver != null) solver.enabled = false;

        while (transform.localScale.x > 0)
        {
            transform.position -= Vector3.up * Time.deltaTime;
            transform.position -= Camera.main.transform.forward * Time.deltaTime;
            transform.localScale -= Vector3.one * Time.deltaTime * 3;
            yield return null;
        }

        Destroy(gameObject);
    }
}
