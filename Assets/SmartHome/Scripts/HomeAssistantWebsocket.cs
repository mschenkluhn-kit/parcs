using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net.WebSockets;
using System.Threading;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.Assertions;

public class HomeAssistantWebsocket
{
    private int currentId = 1;
    private Dictionary<int, SubscriptionResponseHandler> subscriptions = new Dictionary<int, SubscriptionResponseHandler>();

    private ClientWebSocket ws;

    private bool sendingBlocked;

    public WebSocketState ConnectionState { get; internal set; }

    // private SemaphoreSlim sendingSemaphore = new SemaphoreSlim(1, 1);

    public HomeAssistantWebsocket()
    {
        ConnectionState = WebSocketState.None;
    }

    public async Task ConnectAsync(HomeAssistantCredentials credentials)
    {
        ws = new ClientWebSocket();
        ConnectionState = WebSocketState.Connecting;
        await ws.ConnectAsync(new Uri(credentials.homeAssistantServerUrl.Replace("http", "ws") + "/api/websocket"), System.Threading.CancellationToken.None);

        string response = await ReceiveAsync();

        string authMessage = "{\"type\": \"auth\", \"access_token\": \"" + credentials.homeAssistantApiKey + "\"}";
        await Send(authMessage);

        response = await ReceiveAsync();
        ConnectionState = WebSocketState.Open;

        while (true)
        {
            try
            {
                response = await ReceiveAsync();

                HomeAssistantEventResponseData data = JsonConvert.DeserializeObject<HomeAssistantEventResponseData>(response);

                if (data.type != null && data.type == "event")
                {
                    HandleEventResponse(data);
                }
            } catch (Exception e)
            {
            }
        }
    }

    private async Task Send(string message)
    {
        //await sendingSemaphore.WaitAsync();
        //try
        //{
        while (sendingBlocked)
        {
            await Task.Delay(100);
        }

        sendingBlocked = true;
        await ws.SendAsync(new ArraySegment<byte>(System.Text.Encoding.UTF8.GetBytes(message)), WebSocketMessageType.Text, true, System.Threading.CancellationToken.None);
        sendingBlocked = false;
        //}
        //finally
        //{
        //    sendingSemaphore.Release();
        //}
    }

    public async Task Send(object body)
    {
        await Send(JsonConvert.SerializeObject(body));
    }
    
    private async Task<string> ReceiveAsync()
    {
        Assert.IsTrue(ws != null && ws.State == WebSocketState.Open);

        ArraySegment<byte> buffer = new ArraySegment<byte>(new byte[30000]);
        WebSocketReceiveResult result;
        //result = await ws.ReceiveAsync(buffer, CancellationToken.None);
        //byte[] message = buffer.Array;
        byte[] message;
        try
        {
            using (var ms = new MemoryStream())
            {
                do
                {
                    result = await ws.ReceiveAsync(buffer, CancellationToken.None);
                    ms.Write(buffer.Array, buffer.Offset, result.Count);
                } while (!result.EndOfMessage);

                ms.Seek(0, SeekOrigin.Begin);

                message = ms.ToArray();
            }
        }
        catch (Exception e)
        {
            return "";
        }

        return System.Text.Encoding.UTF8.GetString(message);
    }

    public delegate void SubscriptionResponseHandler(HomeAssistantEventResponseData response);

    public async Task Subscribe(HomeAssistantSubscriptionData body, SubscriptionResponseHandler responseHandler)
    {
        body.id = currentId++;

        lock (subscriptions)
        {
            subscriptions.Add(body.id, responseHandler);
        }

        await Send(body);
    }

    private void HandleEventResponse(HomeAssistantEventResponseData response)
    {
        if (subscriptions.ContainsKey(response.id)) {
            subscriptions[response.id](response);
        }
    }
}
