using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomeAssistantServiceData
{
    public string entity_id;
}

public class HomeAssistantServiceDataLightOn: HomeAssistantServiceData
{
    public int brightness; // 0 - 255
    public int color_temp_kelvin;
}

public class HomeAssistantServiceDataLightOff : HomeAssistantServiceData
{
}

public class HomeAssistantServiceDataTemperature : HomeAssistantServiceData
{
    public float temperature;
}

public class HomeAssistantServiceDataDaliLight : HomeAssistantServiceData
{
    public string device;
    public string zone;
    public int intensity;
}

public class HomeAssistantServiceDataDaliBlind : HomeAssistantServiceData
{
    public int position;
}

public class HomeAssistantServiceDataDaliLights : HomeAssistantServiceData
{
    public int intensity;
}

public class HomeAssistantServiceDataDaliScene : HomeAssistantServiceData
{
    public int scene;
}


