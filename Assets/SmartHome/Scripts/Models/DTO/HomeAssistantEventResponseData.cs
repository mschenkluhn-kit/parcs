
using Newtonsoft.Json;
using System.Runtime.Serialization;

public class HomeAssistantEventResponseData
{
    public int id;
    public string type;

    public HomeAssistantEvent @event;

    [JsonProperty("event/variables/trigger/platform")]
    public string platform;

    public class HomeAssistantEvent
    {
        public Variables variables;
        public HomeAssistantEntityData.Context context;
    }
    public class Variables
    {
        public Trigger trigger;
    }
    public class Trigger
    {
        public string id;
        public string idx;
        public string alias;
        public string platform;
        public string entity_id;
        public HomeAssistantEntityData from_state;
        public HomeAssistantEntityData to_state;
    }
}
