using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomeAssistantConfigData
{
    public double latitude { get; set; }
    public double longitude { get; set; }
    public int elevation { get; set; }
    public UnitSystem unit_system { get; set; }
    public string location_name { get; set; }
    public string time_zone { get; set; }
    public List<string> components { get; set; }
    public string config_dir { get; set; }
    public List<string> whitelist_external_dirs { get; set; }
    public List<string> allowlist_external_dirs { get; set; }
    public List<object> allowlist_external_urls { get; set; }
    public string version { get; set; }
    public string config_source { get; set; }
    public bool safe_mode { get; set; }
    public string state { get; set; }
    public object external_url { get; set; }
    public object internal_url { get; set; }
    public string currency { get; set; }
    public string country { get; set; }
    public string language { get; set; }

    public class UnitSystem
    {
        public string length { get; set; }
        public string accumulated_precipitation { get; set; }
        public string mass { get; set; }
        public string pressure { get; set; }
        public string temperature { get; set; }
        public string volume { get; set; }
        public string wind_speed { get; set; }
    }


}
