public class HomeAssistantSubscriptionData
{
    public int id;
    public string type;
    public Trigger trigger;

    public class Trigger
    {
        public string platform;
        public string entity_id;
    }
}
