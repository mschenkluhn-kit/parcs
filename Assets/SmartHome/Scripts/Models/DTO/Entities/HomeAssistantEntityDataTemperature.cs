using System;

public class HomeAssistantEntityDataTemperature
{
    public class Attributes
    {
        public string state_class;
        public string unit_of_measurement;
        public string device_class;
        public string icon;
        public string friendly_name;
        public string temperature;
        public string current_temperature;
        public string current_humidity;
    }

    public class Context
    {
        public string id;
        public object parent_id;
        public object user_id;
    }
}