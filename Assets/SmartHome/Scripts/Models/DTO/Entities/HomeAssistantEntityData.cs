using Newtonsoft.Json.Linq;
using System;
using UnityEngine;

public class HomeAssistantEntityData
{
    public string entity_id;
    public string state;
    public Attributes attributes;
    public DateTime? last_changed;
    public DateTime? last_updated;
    public Context context;

    public HomeAssistantEntityData()
    {
        attributes = new Attributes();
    }

    // Identity based on eId and state
    public override bool Equals(object obj)
    {
        if (obj == null || GetType() != obj.GetType())
        {
            return false;
        }

        bool equal = false;
        HomeAssistantEntityData other = obj as HomeAssistantEntityData;

        if (entity_id.Equals(other.entity_id) &&
            state.Equals(other.state))
        {
            equal = true;
        }

        return equal;
    }

    public override int GetHashCode()
    {
        return base.GetHashCode();
    }

    public class Attributes
    {
        //public string state_class;
        //public string unit_of_measurement;
        public string device_class;
        public string icon;
        public string friendly_name;
    }

    public class Context
    {
        public string id;
        public object parent_id;
        public object user_id;
    }
}