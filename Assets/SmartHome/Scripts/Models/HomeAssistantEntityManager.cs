using Microsoft.MixedReality.Toolkit.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

// Make this class a singleton
public class HomeAssistantEntityManager : MonoBehaviour
{
    [SerializeField]
    private HomeAssistantConnector homeAssistantConnector;

    [SerializeField]
    private GameObject entityPrefab;

    private static HomeAssistantEntityManager instance;
    private bool setupModeActive = false;

    public UnityEvent SetupModeChangeEvent;

    public bool SetupModeActive { get => setupModeActive; 
        set
        {
            if (value == setupModeActive)
            {
                return;
            }

            setupModeActive = value;

            SetupModeChangeEvent.Invoke();

            // Set all ObjectManipulators of all HomeAssistantEntities in the scene to the new value
            HomeAssistantEntity[] entities = FindObjectsOfType<HomeAssistantEntity>();
            foreach (HomeAssistantEntity entity in entities)
            {
                entity.GetComponent<ObjectManipulator>().enabled = setupModeActive;
            }
        } 
    }

    public static HomeAssistantEntityManager GetInstance()
    {
        return instance;
    }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        } else
        {
            Debug.LogWarning("Multiple instances of HomeAssistantEntityManager detected.");
        }
    }
    
    public HomeAssistantEntity InstantiateEntity(HomeAssistantEntityData entityData)
    {
        GameObject entityObject = Instantiate(entityPrefab);
        HomeAssistantEntity entity = entityObject.GetComponent<HomeAssistantEntity>();
        entity.Name = entityData.attributes.friendly_name;
        entity.EntityId = entityData.entity_id;
        entity.State = entityData;

        entityObject.transform.position = Camera.main.transform.position + Camera.main.transform.forward * 1;
        entityObject.SetActive(true);
        entityObject.GetComponent<ObjectManipulator>().enabled = SetupModeActive;

        return entity;
    }

    public void ToggleSetupMode()
    {
        SetupModeActive = !SetupModeActive;
    }


}
