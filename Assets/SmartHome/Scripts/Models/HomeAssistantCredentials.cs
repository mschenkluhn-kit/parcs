using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomeAssistantCredentials
{
    public string homeAssistantServerUrl;
    public string homeAssistantApiKey;
}
