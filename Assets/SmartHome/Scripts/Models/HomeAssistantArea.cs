public class HomeAssistantArea
{
    public string Id { get; set; }

    public string Name { get; set; }

    public HomeAssistantArea() {}

    public HomeAssistantArea(string id, string name)
    {
        Id = id;
        Name = name;
    }

    public HomeAssistantArea(HomeAssistantAreaData data)
    {
        Id = data.area_id;
        Name = data.area_name;
    }
}   
