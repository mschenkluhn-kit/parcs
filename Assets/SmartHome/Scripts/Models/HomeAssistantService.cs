using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using static UnityEngine.EventSystems.EventTrigger;

[RequireComponent(typeof(HomeAssistantEntity))]
public class HomeAssistantService : MonoBehaviour
{
    [Header("Home Assistant Service")]
    
    [SerializeField]
    private HomeAssistantConnector.Domain domain;

    [SerializeField]
    private HomeAssistantConnector.Service service;

    private HomeAssistantEntity entity;

    public HomeAssistantConnector.Service Service { get => service; set => service = value; }
    public HomeAssistantConnector.Domain Domain { get => domain; set => domain = value; }

    private void Start()
    {
        entity = GetComponent<HomeAssistantEntity>();
    }

    public void Run()
    {
        StartCoroutine(RunCoroutine());
    }

    public IEnumerator RunCoroutine()
    {
        HomeAssistantServiceData serviceData;
        if (entity.IsDaliLight)
        {
            serviceData = new HomeAssistantServiceDataDaliLight
            {
                device = entity.EntityId,
                zone = entity.DaliZone,
                intensity = entity.State.state == "on" ? 0 : 100
            };
            entity.State.state = entity.State.state == "on" ? "off" : "on";

        }
        else
        {
            serviceData = new HomeAssistantServiceData();
            serviceData.entity_id = entity.EntityId;
        }
        yield return StartCoroutine(HomeAssistantConnector.GetInstance().CallService(domain, service, serviceData));
    }

    public IEnumerator SetDaliLightIntensity(int intensity)
    {
        if (entity.IsDaliLight)
        {
            HomeAssistantServiceData serviceData = new HomeAssistantServiceDataDaliLight
            {
                device = entity.EntityId,
                zone = entity.DaliZone,
                intensity = intensity
            };
            entity.State.state = "on";

            yield return StartCoroutine(HomeAssistantConnector.GetInstance().CallService(domain, service, serviceData));
        }
        
    }
}
