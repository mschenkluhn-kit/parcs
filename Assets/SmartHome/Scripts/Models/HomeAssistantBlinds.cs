using Microsoft.MixedReality.Toolkit.UI;
using UnityEngine;

[RequireComponent(typeof(HomeAssistantEntity))]
[RequireComponent(typeof(HomeAssistantService))]
public class HomeAssistantBlinds : MonoBehaviour
{
    private HomeAssistantEntity entity;
    private HomeAssistantService service;

    private void Start()
    {
        entity = GetComponent<HomeAssistantEntity>();
        service = GetComponent<HomeAssistantService>();
    }

    public void SetDaliBlindPosition(SliderEventData eventData)
    {
        Debug.Log(eventData.NewValue);
        SetDaliBlindPosition(Mathf.RoundToInt(eventData.NewValue * 100));
    }

    public void SetDaliBlindPosition(int position)
    {
        HomeAssistantServiceData serviceData = new HomeAssistantServiceDataDaliBlind
        {
            entity_id = entity.EntityId,
            position = position
        };

        StartCoroutine(HomeAssistantConnector.GetInstance().CallService(service.Domain, service.Service, serviceData));
    }

}
