using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomeAssistantAreaManager : MonoBehaviour
{
    [SerializeField]
    private HomeAssistantConnector homeAssistantConnector;

    private List<HomeAssistantAreaData> areas;

    private HomeAssistantArea estimatedCurrentArea;
    public HomeAssistantArea CurrentArea { get; set; }
    public HomeAssistantArea EstimatedCurrentArea
    {
        get
        {
            //Debug.LogWarning("Mocking Current Area");
            //return new HomeAssistantArea { Name = "Schlafzimmer", Id = "f80f3550b753750b76cd8cbdb22b74bd" };
            //return new HomeAssistantArea { Name = "K�che", Id = "f80f3550b753750b76cd8cbdb22b74bd" };
            return estimatedCurrentArea;
        }
        set => estimatedCurrentArea = value;
    }

    private void Start()
    {
        StartCoroutine(UpdateAreas());
    }

    public IEnumerator UpdateAreas()
    {
        yield return StartCoroutine(homeAssistantConnector.RunTemplate<List<HomeAssistantAreaData>>(
            new HomeAssistantTemplateData { template = "{% set areas = states | selectattr('attributes.device_class', 'defined') | map(attribute='entity_id') | map('area_id') | unique | reject('none') | list %}[{% for aid in areas %}  {\"area_id\": \"{{ aid }}\",\"area_name\": \"{{ area_name(aid) }}\"},{% endfor %}]" },
            HandleAreaData
            ));
    }

    private void HandleAreaData(List<HomeAssistantAreaData> areas)
    {
        this.areas = areas;

        //foreach (HomeAssistantAreaData area in areas)
        //{
        //    Debug.Log(string.Format("{0} ({1})", area.area_name, area.area_id));
        //}

    }

    /// <summary>
    /// Retrieves the area of the motion sensor that triggered the most recent.
    /// Result is stored in "EstimatedCurrentArea"
    /// </summary>
    public IEnumerator EstimateCurrentArea()
    {
        yield return StartCoroutine(homeAssistantConnector.RunTemplate<HomeAssistantEntityAreaData>(
            new HomeAssistantTemplateData { template = "{% set eid = states | selectattr('attributes.device_class', 'defined') | selectattr('attributes.device_class', 'eq', 'motion') | sort(attribute = 'last_changed', reverse=true) | map(attribute='entity_id') | first %}{    \"entity_id\": \"{{ eid }}\",    \"area_id\": \"{{ area_id(eid) }}\"}" },
            HandleEntityAreaDataObject
            ));
    }

    private void HandleEntityAreaDataObject(HomeAssistantEntityAreaData entity)
    {
        if (entity.entity_id.Equals("") && entity.area_id.Equals("None"))
        {
            Debug.LogWarning("No motion sensor found. Picking first area.");
            EstimatedCurrentArea = new HomeAssistantArea { Id = areas[0].area_id, Name = areas[0].area_name };
            return;
        }

        for (int i = 0; i < areas.Count; i++)
        {
            if (areas[i].area_id == entity.area_id)
            {
                EstimatedCurrentArea = new HomeAssistantArea { Id = entity.area_id, Name = areas[i].area_name };
            }
        }
    }

}
