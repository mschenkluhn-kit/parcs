using System;
using System.Collections;
using System.Diagnostics;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class HomeAssistantEntity : MonoBehaviour
{
    [SerializeField]
    private string entityName;

    [SerializeField]
    private string entityId;

    [SerializeField]
    private bool isDaliLight = false;

    [SerializeField]
    private string daliZone;

    [SerializeField]
    private bool isUpdatingState = true;

    private HomeAssistantEntityData state;
    private HomeAssistantEntityData newState;

    public UnityEvent onStateChange;

    public string Name { get => entityName; set => entityName = value; }
    public string EntityId { get => entityId; set => entityId = value; }

    public string DaliZone { get => daliZone; set => daliZone = value; }

    public bool IsDaliLight { get => isDaliLight; set => isDaliLight = value; }

    public HomeAssistantEntityData State { 
        get => state; 
        set {
            if (state == null && value != null || !state.Equals(value))
            {
                state = value;

                if (onStateChange != null)
                    onStateChange.Invoke();
            }

            if ((Name == null || Name.Length == 0) && state.attributes != null && state.attributes.friendly_name != null)
            {
                Name = state.attributes.friendly_name;
            }
        }
    }

    public bool IsUpdatingState { get => isUpdatingState;
        set
        {
            isUpdatingState = value;
            
            if (isUpdatingState)
            {
                UpdateStateOnce();
            }
        }
    }

    public IEnumerator UpdateStateOnce()
    {
        yield return StartCoroutine(UpdateState());
    }

    private void Start()
    {
        if (isDaliLight)
        {
            State = new HomeAssistantEntityData();
            state.state = "off";
        } else
        {
            StartCoroutine(UpdateStateOnce());
        }

        //InitWSUpdate();
    }

    private void Update()
    {
        // Return newState information to main thread
        if (newState != null)
        {
            State = newState;
            newState = null;
        }
    }

    private void InitWSUpdate()
    {
        HomeAssistantSubscriptionData subscriptionData = new HomeAssistantSubscriptionData
        {
            type = "subscribe_trigger",
            trigger = new HomeAssistantSubscriptionData.Trigger
            {
                entity_id = EntityId,
                platform = "state"
            }
        };
        HomeAssistantConnectorWS.GetInstance().Subscribe(subscriptionData, UpdateStateWSHandler);
        StartCoroutine(UpdateState());
    }
    private void UpdateStateWSHandler(HomeAssistantEventResponseData response)
    {
        if (isUpdatingState && response != null && response.@event.variables.trigger.to_state != null)
        {
            HomeAssistantEntityData state = response.@event.variables.trigger.to_state;
            newState = state;
        }
    }

    private IEnumerator UpdateState()
    {
        yield return StartCoroutine(HomeAssistantConnector.GetInstance().UpdateState(this, state =>
        {
            if (state != null)
            {
                State = state;
            }
        }));
    }

    public void DisplayState(TextMeshPro tmp)
    {
        if (state != null)
        {
            // tmp.text = "" + state.state + " " + (state.attributes.unit_of_measurement ?? "");
            tmp.text = "" + state.state;
        } else
        {
            tmp.text = "n.A.";
        }
    }

}
