using Microsoft.MixedReality.Toolkit.Input;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(EyeTrackingTarget))]
[RequireComponent(typeof(HomeAssistantEntity))]
[RequireComponent(typeof(HomeAssistantService))]
public class TargetInteraction : MonoBehaviour, IMixedRealityGestureHandler, IMixedRealityPointerHandler
{
    [Header("Colors")]

    [SerializeField]
    private Color stateOffColor;

    [SerializeField]
    private Color stateOnColor;

    [SerializeField]
    private Color stateSelectColor;

    [SerializeField]
    [Range(0, 1f)]
    private float defaultOpacity = .5f;

    [SerializeField]
    [Range(0, 1f)]
    private float hoverOpacity = 1f;


    [Header("Actions")]

    public UnityEvent onSelect;

    [SerializeField]
    private bool requireLookAt = true;


    private HomeAssistantEntity entity;
    private HomeAssistantService service;

    void Start()
    {
        GetComponent<EyeTrackingTarget>().OnLookAtStart.AddListener(OnLookAtStart);
        GetComponent<EyeTrackingTarget>().OnLookAway.AddListener(OnLookAway);
        entity = GetComponent<HomeAssistantEntity>();
        service = GetComponent<HomeAssistantService>();

        entity.onStateChange.AddListener(SetStateColor);

        SetStateColor();
    }

    public void SetSelectColor()
    {
        SetColor(stateSelectColor);
    }

    public void SetStateColor()
    {
        if (entity.State != null && entity.State.state != null && entity.State.state.Equals("on"))
        {
            SetColor(stateOnColor);
        }
        else
        {
            SetColor(stateOffColor);
        }
    }

    private void SetColor(Color color, float opacity = -1)
    {
        if (opacity < 0)
        {
            if (IsLookedAt())
            {
                color.a = hoverOpacity;
            }
            else
            {
                color.a = defaultOpacity;
            }
        }
        else
        {
            color.a = opacity;
        }
        gameObject.GetComponent<Renderer>().material.color = color;
    }

    void SetOpacity(float opacity)
    {
        var material = gameObject.GetComponent<Renderer>().material;
        Color c = material.color;
        c.a = opacity;
        material.color = c;
    }

    #region Eye Tracking Handling

    void OnLookAtStart()
    {
        Debug.Log("Look at.");
        SetOpacity(hoverOpacity);
    }

    void OnLookAway()
    {
        Debug.Log("Look away.");
        SetStateColor();
        SetOpacity(defaultOpacity);
    }

    bool IsLookedAt()
    {
        return GetComponent<EyeTrackingTarget>().IsLookedAt;
    }


    #endregion


    #region Gesture Handling
    void IMixedRealityGestureHandler.OnGestureStarted(InputEventData eventData)
    {
        //if (IsLookedAt())
        //{
        //    SetSelectColor();
        //}
        Debug.Log("GestureHandler Started");
    }

    void IMixedRealityGestureHandler.OnGestureUpdated(InputEventData eventData) { }

    void IMixedRealityGestureHandler.OnGestureCompleted(InputEventData eventData)
    {
        //if (IsLookedAt())
        //{
        //    HomeAssistantServiceData homeAssistantServiceData = new HomeAssistantServiceData();
        //    homeAssistantServiceData.entity_id = entity.EntityId;
        //    StartCoroutine(HomeAssistantConnector.GetInstance().CallService(domain, service, homeAssistantServiceData));

        //    SetStateColor();

        //    onSelect.Invoke();
        //}
        Debug.Log("GestureHandler Completed");
    }

    void IMixedRealityGestureHandler.OnGestureCanceled(InputEventData eventData) { }

    void IMixedRealityPointerHandler.OnPointerDown(MixedRealityPointerEventData eventData)
    {
        if (!HomeAssistantEntityManager.GetInstance().SetupModeActive)
        {
            if (IsLookedAt() || !requireLookAt)
            {
                SetSelectColor();
            }
        }
        Debug.Log("PointerHandler Down");
    }

    void IMixedRealityPointerHandler.OnPointerDragged(MixedRealityPointerEventData eventData) { }

    void IMixedRealityPointerHandler.OnPointerUp(MixedRealityPointerEventData eventData)
    {
        // Only trigger action if not in setup mode
        if (!HomeAssistantEntityManager.GetInstance().SetupModeActive)
        {
            if (IsLookedAt() || !requireLookAt)
            {
                service.Run();

                SetStateColor();

                onSelect.Invoke();
            }
        }
        Debug.Log("PointerHandler Up");
    }

    void IMixedRealityPointerHandler.OnPointerClicked(MixedRealityPointerEventData eventData) { }
    #endregion
}
