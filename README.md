# PARCS: Prototypical Augmented Reality Configuration System

PARCS (Prototypical Augmented Reality Configuration System) is a system designed to facilitate the setup of smart homes through augmented reality (AR) technology. This prototype was developed for studying the spatial setup and control of smart home devices. The study was published at CHI 2024 [1].


## Features

- **Manufacturer-Independent:** PARCS is designed to be compatible with a wide range of smart home devices, regardless of manufacturer or model.
  
- **Automatic Position Detection:** The system utilizes the combination of actuators present in smart home appliances and the sensing capabilities of Head Mounted Displays (HMDs) to automatically detect the positions of IoT devices.
  
- **Distinctive Signal Recognition:** Each smart home device emits a distinctive signal, which can include switching LED power indicators, emitting specific sounds, or visually distinctive movements. PARCS leverages the integrated cameras and microphones in HMDs to detect and interpret these signals.
  
- **Intuitive Interface:** PARCS provides users with an intuitive AR interface for configuring and controlling their smart home devices. Through simple gestures and commands, users can interact with their IoT devices in a more natural and immersive way.

## Requirements
To use PARCS, install the [Mixed Reality Toolkit 2.8.3](https://learn.microsoft.com/en-us/windows/mixed-reality/develop/install-the-tools). Additionally, [OpenCV for HoloLens](https://github.com/EnoxSoftware/HoloLensWithOpenCVForUnityExample) is used for the smart light detection.

## References

[1] Marius Schenkluhn, Michael Knierim, Francisco Kiss, and Christof Weinhardt. 2024. Connecting Home: Human-Centric Setup Automation in the Augmented Smart Home. In CHI ’24: ACM CHI conference on Human Factors in Computing Systems, May 11–16, 2024, Hawaii, USA. ACM, New York, NY, USA, 15 pages. [DOI: 10.1145/3613904.3642862](https://doi.org/10.1145/3613904.3642862)